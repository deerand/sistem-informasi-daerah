<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
      </div>
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="header">
              <h2>Profil Desa</h2>
            </div>
            <div class="body">
              <ol class="breadcrumb breadcrumb-bg-blue">
                <li class="active"><i class="material-icons">info</i> Untuk melakukan perubahan data silahkan klik data
                  informasi.</li>
              </ol>
              <div class="bs-example" data-example-id="media-alignment">
                <div class="media">
                  <div class="media-left">
                    <a href="javascript:void(0);">
                      <img class="media-object" src="../../../images/user-img-background.jpg" width="100" height="100">
                    </a>
                  </div>
                  <div class="media-body">
                    <form id="form_edit_profilDesa" method="POST" enctype="multipart/form-data">
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for='inputNamaDesa'>Nama Desa</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="nama_desa" placeholder="Nama Desa" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for='inputKodeDesa'>Kode Desa</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="kode_desa" placeholder="Kode Desa" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                        <div class="form-group">
                          <label for='inputNomerDesa'>Nomer Desa</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="nomer_desa" placeholder="Nomer Desa" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                          <label for='inputNamaJalan'>Nama Jalan</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="nama_jalan" placeholder="Nama Jalan" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for='inputNomerTlp'>Nomor Telepon</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="nomer_telepon" placeholder="Nomer Telepon" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for='inputFax'>Fax</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="fax" placeholder="Fax" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for='inputWebsiteDesa'>Website Desa</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="website_desa" placeholder="Website Desa" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                        <div class="form-group">
                          <label for='inputKodePos'>Kode Pos</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="kode_pos" placeholder="Kode Pos" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                          <label for='inputDisdukcapil'>Nama Disdukcapil</label>
                          <div class="form-line">
                            <input type="text" class="form-control" name="disduk_capil" placeholder="Nama Diskdukcapil" />
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                          <label for='InputAlamat'>Alamat</label>
                          <div class="form-line">
                            <textarea rows="1" class="form-control no-resize auto-growth" name="alamat" placeholder="Alamat"></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="form-group">
                          <label for='InputNamaAyah'>Foto</label>
                          <div class="form-line">
                            <input type="file" class="form-control" name="image_edit" id="image_edit" />
                          </div>
                        </div>
                        <input type="submit" class="btn btn-primary waves-effect" value="Edit">
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- #END# Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="body bg-pink">
              <div class="font-bold m-b--35">Informasi</div>
              <ul class="dashboard-stat-list">
                <li>
                  Data pegawai Desa harus valid. Silahkan lengkapi data disamping dengan data sebenarnya.
                </li>
              </ul>
            </div>
          </div>
        </div>
  </section>

  <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>