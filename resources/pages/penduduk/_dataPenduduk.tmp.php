<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
	<?php include_once "../../../resources/partrials/_header.php"; ?>
	<?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
	<!-- Page Loader -->
	<?php include_once "../../../resources/partrials/_loader.php"; ?>
	<!-- #END# Page Loader -->
	<!-- Top Bar -->
	<?php include "../../../resources/partrials/_topbar.php"; ?>
	<!-- #Top Bar -->
	<section>
		<!-- Left Sidebar -->
		<aside id="leftsidebar" class="sidebar">
			<!-- User Info -->
			<?php include_once "../../../resources/partrials/_userinfo.php"; ?>
			<!-- #User Info -->
			<!-- Menu -->
			<?php include_once "../../../resources/partrials/_sidebar.php"; ?>
			<!-- #Menu -->
			<!-- Footer -->
			<?php include_once "../../../resources/partrials/_footer.php"; ?>
			<!-- #Footer -->
		</aside>
		<!-- #END# Left Sidebar -->
	</section>

	<section class="content">
		<div class="container-fluid">
			<div class="block-header">
				<h2>Data Penduduk Yang Sudah Terdaftar</h2>
			</div>
			<div class="row clearfix">
				<!-- Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
					<div class="card">
						<div class="header">
							<h2>Data Penduduk</h2>
							<button type="button" class="btn btn-primary waves-effect" data-toggle="modal" data-target="#modal_add_penduduk">Tambah
								Penduduk</button>
						</div>
						<div class="body">
							<div class="table-responsive">
								<table class="table table-hover dashboard-task-infos" id="datatable_data_penduduk">
									<thead>
										<tr>
											<th>NO</th>
											<th>NIK</th>
											<th>Nama Lengkap</th>
											<th>JK</th>
											<th>Tempat Lahir</th>
											<th>Tanggal Lahir</th>
											<th>Actions</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="modal fade" id="modal_add_penduduk" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="defaultModalLabel">Tambah Penduduk</h4>
							</div>
							<div class="modal-body">
								<form id="form_add_penduduk" method="post" enctype="multipart/form-data">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group">
											<div class="form-line">
												<label for='InputNIK'>NIK</label>
												<input type="text" class="form-control" name="nik" placeholder="NIK" id="nik" required />
											</div>
										</div>
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
										<div class="form-group">
											<label for='InputNama'>Nama Lengkap</label>
											<div class="form-line">
												<input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Lengkap" required />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAlamat'>Alamat</label>
											<div class="form-line">
												<textarea rows="1" class="form-control no-resize auto-growth" name="alamat" placeholder="Alamat" id="alamat"
												 required></textarea>
											</div>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group">
											<label for='InputJK'>Jenis Kelamin</label>
											<div class="form-line">
												<select id="jk" class="form-control show-tick" name="jk">
													<option value="">---Pilih---</option>
													<option value="P">Pria</option>
													<option value="W">Wanita</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group">
											<label for='InputTmpLahir'>Tempat Lahir</label>
											<div class="form-line">
												<input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" id="tmp_lahir" required />
											</div>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group">
											<label for='InputTglLahir'>Tanggal Lahir</label>
											<div class="form-line">
												<input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datepicker" placeholder="Tanggal Lahir"
												 required />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAkteLahir'>Akte Kelahiran</label>
											<div class="form-line">
												<select id="aktelahir" class="form-control show-tick" name="aktelahir">
													<option value="">---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fix_no_akte_lahir" style="display:none;">
										<div class="form-group">
											<label for='InputNoAkteLahir'>No Akte Kelahiran</label>
											<div class="form-line">
												<input type="text" class="form-control" id="no_aktelahir" name="no_aktelahir" placeholder="No Akte Kelahiran"
												 required />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAgama'>Agama</label>
											<div class="form-line">
												<select class="form-control show-tick" name="agama" id="agama" required>
													<option value="">--- Pilih ---</option>
													<option value="Islam">Islam</option>
													<option value="Kristen">Kristen</option>
													<option value="Katolik">Katolik</option>
													<option value="Hindu">Hindu</option>
													<option value="Budha">Budha</option>
													<option value="Khonghucu">Khonghucu</option>
													<option value="Kepercayaan">Kepercayaan</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
										<div class="form-group">
											<label for='InputPendidikan'>Pendidikan</label>
											<div class="form-line">
												<select class="form-control show-tick" name="pendidikan" id="pendidikan" required>
													<option value="">--- Pilih ---</option>
													<option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
													<option value="Belum Tamat SD/Sederajat">Belum Tamat SD/Sederajat</option>
													<option value="Tamat SD/Sederajat">Tamat SD/Sederajat</option>
													<option value="SLTP/Sederajat">SLTP/Sederajat</option>
													<option value="SLTA/Sederajat">SLTA/Sederajat</option>
													<option value="Diploma I/II">Diploma I/II</option>
													<option value="Akademi/Diploma III/Sarjana Muda">Akademi/Diploma III/Sarjana Muda</option>
													<option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
													<option value="Strata II">Strata II</option>
													<option value="Strata III">Strata III</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
										<div class="form-group">
											<label for='InputGolDar'>Golongan Darah</label>
											<div class="form-line">
												<select class="form-control show-tick" name="goldar" id="goldar">
													<option value="">--- Pilih ---</option>
													<option value="A">A</option>
													<option value="B">B</option>
													<option value="AB">AB</option>
													<option value="O">O</option>
													<option value="A">A-</option>
													<option value="A+">A+</option>
													<option value="B-">B-</option>
													<option value="B+">B+</option>
													<option value="AB-">AB-</option>
													<option value="AB+">AB+</option>
													<option value="O-">O-</option>
													<option value="O+">O+</option>
													<option value="Tidak Tahu">Tidak Tahu</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
										<div class="form-group">
											<label for='InputPekerjaan'>Pekerjaan</label>
											<div class="form-line">
												<select class="form-control show-tick" name="pekerjaan" id="pekerjaan">
													<option value="">--- Pilih ---</option>
													<option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja</option>
													<option value="Mengurus Rumah Tangga">Mengurus Rumah Tangga</option>
													<option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
													<option value="Pensiunan">Pensiunan</option>
													<option value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS)</option>
													<option value="Tentara Nasional Indonesia (TNI)">Tentara Nasional Indonesia (TNI)</option>
													<option value="Kepolisian RI (POLRI)">Kepolisian RI (POLRI)</option>
													<option value="Perdagangan">Perdagangan</option>
													<option value="Petani/Pekebun">Petani/Pekebun</option>
													<option value="Peternak">Peternak</option>
													<option value="Nelayan/Perikanan">Nelayan/Perikanan</option>
													<option value="Industri">Industri</option>
													<option value="Konstruksi">Konstruksi</option>
													<option value="Transportasi">Transportasi</option>
													<option value="Karyawan Swasta">Karyawan Swasta</option>
													<option value="Karyawan BUMN">Karyawan BUMN</option>
													<option value="Karyawan BUMD">Karyawan BUMD</option>
													<option value="Karyawan Honorer">Karyawan Honorer</option>
													<option value="Buruh Harian Lepas">Buruh Harian Lepas</option>
													<option value="Buruh Tani/Perkebunan">Buruh Tani/Perkebunan</option>
													<option value="Buruh Nelayan/Perikanan">Buruh Nelayan/Perikanan</option>
													<option value="Buruh Peternakan">Buruh Peternakan</option>
													<option value="Pembantu Rumah Tangga">Pembantu Rumah Tangga</option>
													<option value="Tukang Cukur">Tukang Cukur</option>
													<option value="Tukang Listrik">Tukang Listrik</option>
													<option value="Tukang Batu">Tukang Batu</option>
													<option value="Tukang Kayu">Tukang Kayu</option>
													<option value="Tukang Sol Sepatu">Tukang Sol Sepatu</option>
													<option value="Tukang Las/Pandai Besi">Tukang Las/Pandai Besi</option>
													<option value="Tukang Jahit">Tukang Jahit</option>
													<option value="Tukang Gigi">Tukang Gigi</option>
													<option value="Penata Rias">Penata Rias</option>
													<option value="Penata Busana">Penata Busana</option>
													<option value="Penata Rambut">Penata Rambut</option>
													<option value="Mekanik">Mekanik</option>
													<option value="Seniman">Seniman</option>
													<option value="Tabib">Tabib</option>
													<option value="Paraji">Paraji</option>
													<option value="Perancang Busana">Perancang Busana</option>
													<option value="Penterjemah">Penterjemah</option>
													<option value="Imam Masjid">Imam Masjid</option>
													<option value="Pendeta">Pendeta</option>
													<option value="Pastor">Pastor</option>
													<option value="Wartawan">Wartawan</option>
													<option value="Ustadz/Mubaligh">Ustadz/Mubaligh</option>
													<option value="Juru Masak">Juru Masak</option>
													<option value="Promotor Acara">Promotor Acara</option>
													<option value="Anggota DPR-RI">Anggota DPR-RI</option>
													<option value="Anggota DPD">Anggota DPD</option>
													<option value="Anggota BPK">Anggota BPK</option>
													<option value="Presiden">Presiden</option>
													<option value="Wakil Presiden">Wakil Presiden</option>
													<option value="Anggota Mahkamah Konstitusi">Anggota Mahkamah Konstitusi</option>
													<option value="Anggota Kabinet/Kementrian">Anggota Kabinet/Kementrian</option>
													<option value="Duta Besar">Duta Besar</option>
													<option value="Gubernur">Gubernur</option>
													<option value="Wakil Gubernur">Wakil Gubernur</option>
													<option value="Bupati">Bupati</option>
													<option value="Wakil Bupati">Wakil Bupati</option>
													<option value="Walikota">Walikota</option>
													<option value="Wakil walikota">Wakil walikota</option>
													<option value="Anggota DPRD Prop.">Anggota DPRD Prop.</option>
													<option value="Anggota DPRD Kab./Kota">Anggota DPRD Kab./Kota</option>
													<option value="Dosen">Dosen</option>
													<option value="Guru">Guru</option>
													<option value="Pilot">Pilot</option>
													<option value="Pengacara">Pengacara</option>
													<option value="Notaris">Notaris</option>
													<option value="Arsitek">Arsitek</option>
													<option value="Akuntan">Akuntan</option>
													<option value="Konsultan">Konsultan</option>
													<option value="Dokter">Dokter</option>
													<option value="Bidan">Bidan</option>
													<option value="Perawat">Perawat</option>
													<option value="Apoteker">Apoteker</option>
													<option value="Psikiater/Psikolog">Psikiater/Psikolog</option>
													<option value="Penyiar Televisi">Penyiar Televisi</option>
													<option value="Penyiar Radio">Penyiar Radio</option>
													<option value="Pelaut">Pelaut</option>
													<option value="Peneliti">Peneliti</option>
													<option value="Sopir">Sopir</option>
													<option value="Pialang">Pialang</option>
													<option value="Pedagang">Pedagang</option>
													<option value="Perangkat Desa">Perangkat Desa</option>
													<option value="Kepala Desa">Kepala desa</option>
													<option value="Biarawati">Biarawati</option>
													<option value="Wiraswasta">Wiraswasta</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
										<div class="form-group">
											<label for='InputPenghasilan'>Penghasilan</label>
											<div class="form-line">
												<input type="text" class="form-control" name="penghasilan" placeholder="Penghasilan" id="penghasilan" />
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputStatusDalam Keluarga'>Status Dalam Keluarga</label>
											<div class="form-line">
												<select class="form-control show-tick" name="sDalamKeluarga" id="sDalamKeluarga">
													<option value="">--- Pilih ---</option>
													<option value="Kepala Keluarga">Kepala keluarga</option>
													<option value="Suami">Suami</option>
													<option value="Istri">Istri</option>
													<option value="Anak">Anak</option>
													<option value="Menantu">Menantu</option>
													<option value="Cucu">Cucu</option>
													<option value="Orang Tua">Orang Tua</option>
													<option value="Mertua">Mertua</option>
													<option value="Famili Lain">Famili Lain</option>
													<option value="Pembantu">Pembantu</option>
													<option value="Lainnya">Lainnya</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputStatusPerkawinan'>Status Pernikahan</label>
											<div class="form-line">
												<select class="form-control show-tick" name="sPerkawinan" id="sPerkawinan">
													<option value="">--- Pilih ---</option>													
													<option value="Belum Kawin">Belum Kawin</option>
													<option value="Kawin">Kawin</option>
													<option value="Cerai">Cerai</option>													
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAkteKawin'>Akte Pernikahan</label>
											<div class="form-line">
												<select id="aktekawin" class="form-control show-tick" name="aktekawin">
													<option value="">---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display:none;" id="fix_no_aktekawin">
										<div class="form-group">
											<label for='InputNoAkteKawin'>No Akte Pernikahan</label>
											<div class="form-line">
												<input type="text" class="form-control" name="no_aktekawin" placeholder="No Akte Perkawinan" id="no_aktekawin" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display:none;" id="fix_tgl_aktekawin">
										<div class="form-group">
											<label for='InputTglKawin'>Tanggal Pernikahan</label>
											<div class="form-line">
												<input type="text" class="form-control datepicker" name="tgl_aktekawin" placeholder="Tanggal Perkawinan" id="tgl_aktekawin" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAkteCerai'>Akte Cerai</label>
											<div class="form-line">
												<select id="aktecerai" class="form-control show-tick" name="aktecerai">
													<option value="">---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display:none;" id="fix_no_aktecerai">
										<div class="form-group">
											<label for='InputNoAkteCerai'>No Akte Perceraian</label>
											<div class="form-line">
												<input type="text" class="form-control" name="no_aktecerai" placeholder="No Akte Perceraian" id="no_aktecerai" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="display:none;" id="fix_tgl_aktecerai">
										<div class="form-group">
											<label for='InputTglCerai'>Tanggal Perceraian</label>
											<div class="form-line">
												<input type="text" class="form-control datepicker" name="tgl_aktecerai" placeholder="Tanggal Percereian" id="tgl_aktecerai" />
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputAkteLahir'>Kelainan FM</label>
											<div class="form-line">
												<select id="kelainanFM" class="form-control show-tick" name="kelainanFM">
													<option>---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputJK'>Status Hidup</label>
											<div class="form-line">
												<select id="status_hidup" class="form-control show-tick" name="status_hidup">
													<option value="">---Pilih---</option>
													<option value="Hidup">Hidup</option>
													<option value="Meninggal">Meninggal</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputNamaAyah'>Foto</label>
											<div class="form-line">
												<input type="file" class="form-control" name="image" />
											</div>
										</div>
									</div>
							</div>
							<div class="modal-footer">
								<input type="submit" class="btn btn-primary waves-effect" value="Tambah">
								<button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
							</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal fade" id="modal_edit_penduduk" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="defaultModalLabel">Edit Penduduk</h4>
							</div>
							<div class="modal-body">
								<form id="form_edit_penduduk" method="post" enctype="multipart/form-data">
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group">
											<div class="form-line">
												<label for='InputNIK'>NIK</label>
												<input type="text" class="form-control" name="nik_edit" placeholder="NIK" id="nik_edit" />
												<input type="hidden" name="id_edit" id="id_edit">
											</div>
										</div>
									</div>
									<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
										<div class="form-group">
											<label for='InputNama'>Nama Lengkap</label>
											<div class="form-line">
												<input type="text" class="form-control" id="nama_edit" name="nama_edit" placeholder="Nama Lengkap" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAlamat'>Alamat</label>
											<div class="form-line">
												<textarea rows="1" class="form-control no-resize auto-growth" name="alamat_edit" placeholder="Alamat" id="alamat_edit"></textarea>
											</div>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 ">
										<div class="form-group">
											<label for='InputJK'>Jenis Kelamin</label>
											<div class="form-line">
												<select id="jk_edit" class="form-control show-tick" name="jk_edit">
													<option value="">---Pilih---</option>
													<option value="P">Pria</option>
													<option value="W">Wanita</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group">
											<label for='InputTmpLahir'>Tempat Lahir</label>
											<div class="form-line">
												<input type="text" class="form-control" name="tmp_lahir_edit" placeholder="Tempat Lahir" id="tmp_lahir_edit" />
											</div>
										</div>
									</div>
									<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
										<div class="form-group">
											<label for='InputTglLahir'>Tanggal Lahir</label>
											<div class="form-line">
												<input type="text" name="tgl_lahir_edit" id="tgl_lahir_edit" class="form-control datepicker" placeholder="Tanggal Lahir">
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAkteLahir'>Akte Kelahiran</label>
											<div class="form-line">
												<select id="aktelahir_edit" class="form-control show-tick" name="aktelahir_edit">
													<option value="">---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fix_no_aktelahir_edit">
										<div class="form-group">
											<label for='InputNoAkteLahir'>No Akte Kelahiran</label>
											<div class="form-line">
												<input type="text" class="form-control" name="no_aktelahir_edit" placeholder="No Akte Kelahiran" id="no_aktelahir_edit" />
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputAgama'>Agama</label>
											<div class="form-line">
												<select id="agama_edit" class="form-control show-tick" name="agama_edit">
													<option value="">--- Pilih ---</option>
													<option value="Islam">Islam</option>
													<option value="Kristen">Kristen</option>
													<option value="Katolik">Katolik</option>
													<option value="Hindu">Hindu</option>
													<option value="Budha">Budha</option>
													<option value="Khonghucu">Khonghucu</option>
													<option value="Kepercayaan">Kepercayaan</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputPendidikan'>Pendidikan</label>
											<div class="form-line">
												<select class="form-control show-tick" name="pendidikan_edit" id="pendidikan_edit">
													<option value="">--- Pilih ---</option>
													<option value="Tidak/Belum Sekolah">Tidak/Belum Sekolah</option>
													<option value="Belum Tamat SD/Sederajat">Belum Tamat SD/Sederajat</option>
													<option value="Tamat SD/Sederajat">Tamat SD/Sederajat</option>
													<option value="SLTP/Sederajat">SLTP/Sederajat</option>
													<option value="SLTA/Sederajat">SLTA/Sederajat</option>
													<option value="Diploma I/II">Diploma I/II</option>
													<option value="Akademi/Diploma III/Sarjana Muda">Akademi/Diploma III/Sarjana Muda</option>
													<option value="Diploma IV/Strata I">Diploma IV/Strata I</option>
													<option value="Strata II">Strata II</option>
													<option value="Strata III">Strata III</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
										<div class="form-group">
											<label for='InputGolDar'>Golongan Darah</label>
											<div class="form-line">
												<select class="form-control show-tick" name="goldar_edit" id="goldar_edit">
													<option value="">Pilih Golongan Darah</option>
													<option value="A">A</option>
													<option value="B">B</option>
													<option value="AB">AB</option>
													<option value="O">O</option>
													<option value="A">A-</option>
													<option value="A+">A+</option>
													<option value="B-">B-</option>
													<option value="B+">B+</option>
													<option value="AB-">AB-</option>
													<option value="AB+">AB+</option>
													<option value="O-">O-</option>
													<option value="O+">O+</option>
													<option value="Tidak Tahu">Tidak Tahu</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
										<div class="form-group">
											<label for='InputPekerjaan'>Pekerjaan</label>
											<div class="form-line">
												<select class="form-control show-tick" name="pekerjaan_edit" id="pekerjaan_edit">
													<option value="">--- Pilih ---</option>
													<option value="Belum/Tidak Bekerja">Belum/Tidak Bekerja</option>
													<option value="Mengurus Rumah Tangga">Mengurus Rumah Tangga</option>
													<option value="Pelajar/Mahasiswa">Pelajar/Mahasiswa</option>
													<option value="Pensiunan">Pensiunan</option>
													<option value="Pegawai Negeri Sipil (PNS)">Pegawai Negeri Sipil (PNS)</option>
													<option value="Tentara Nasional Indonesia (TNI)">Tentara Nasional Indonesia (TNI)</option>
													<option value="Kepolisian RI (POLRI)">Kepolisian RI (POLRI)</option>
													<option value="Perdagangan">Perdagangan</option>
													<option value="Petani/Pekebun">Petani/Pekebun</option>
													<option value="Peternak">Peternak</option>
													<option value="Nelayan/Perikanan">Nelayan/Perikanan</option>
													<option value="Industri">Industri</option>
													<option value="Konstruksi">Konstruksi</option>
													<option value="Transportasi">Transportasi</option>
													<option value="Karyawan Swasta">Karyawan Swasta</option>
													<option value="Karyawan BUMN">Karyawan BUMN</option>
													<option value="Karyawan BUMD">Karyawan BUMD</option>
													<option value="Karyawan Honorer">Karyawan Honorer</option>
													<option value="Buruh Harian Lepas">Buruh Harian Lepas</option>
													<option value="Buruh Tani/Perkebunan">Buruh Tani/Perkebunan</option>
													<option value="Buruh Nelayan/Perikanan">Buruh Nelayan/Perikanan</option>
													<option value="Buruh Peternakan">Buruh Peternakan</option>
													<option value="Pembantu Rumah Tangga">Pembantu Rumah Tangga</option>
													<option value="Tukang Cukur">Tukang Cukur</option>
													<option value="Tukang Listrik">Tukang Listrik</option>
													<option value="Tukang Batu">Tukang Batu</option>
													<option value="Tukang Kayu">Tukang Kayu</option>
													<option value="Tukang Sol Sepatu">Tukang Sol Sepatu</option>
													<option value="Tukang Las/Pandai Besi">Tukang Las/Pandai Besi</option>
													<option value="Tukang Jahit">Tukang Jahit</option>
													<option value="Tukang Gigi">Tukang Gigi</option>
													<option value="Penata Rias">Penata Rias</option>
													<option value="Penata Busana">Penata Busana</option>
													<option value="Penata Rambut">Penata Rambut</option>
													<option value="Mekanik">Mekanik</option>
													<option value="Seniman">Seniman</option>
													<option value="Tabib">Tabib</option>
													<option value="Paraji">Paraji</option>
													<option value="Perancang Busana">Perancang Busana</option>
													<option value="Penterjemah">Penterjemah</option>
													<option value="Imam Masjid">Imam Masjid</option>
													<option value="Pendeta">Pendeta</option>
													<option value="Pastor">Pastor</option>
													<option value="Wartawan">Wartawan</option>
													<option value="Ustadz/Mubaligh">Ustadz/Mubaligh</option>
													<option value="Juru Masak">Juru Masak</option>
													<option value="Promotor Acara">Promotor Acara</option>
													<option value="Anggota DPR-RI">Anggota DPR-RI</option>
													<option value="Anggota DPD">Anggota DPD</option>
													<option value="Anggota BPK">Anggota BPK</option>
													<option value="Presiden">Presiden</option>
													<option value="Wakil Presiden">Wakil Presiden</option>
													<option value="Anggota Mahkamah Konstitusi">Anggota Mahkamah Konstitusi</option>
													<option value="Anggota Kabinet/Kementrian">Anggota Kabinet/Kementrian</option>
													<option value="Duta Besar">Duta Besar</option>
													<option value="Gubernur">Gubernur</option>
													<option value="Wakil Gubernur">Wakil Gubernur</option>
													<option value="Bupati">Bupati</option>
													<option value="Wakil Bupati">Wakil Bupati</option>
													<option value="Walikota">Walikota</option>
													<option value="Wakil walikota">Wakil walikota</option>
													<option value="Anggota DPRD Prop.">Anggota DPRD Prop.</option>
													<option value="Anggota DPRD Kab./Kota">Anggota DPRD Kab./Kota</option>
													<option value="Dosen">Dosen</option>
													<option value="Guru">Guru</option>
													<option value="Pilot">Pilot</option>
													<option value="Pengacara">Pengacara</option>
													<option value="Notaris">Notaris</option>
													<option value="Arsitek">Arsitek</option>
													<option value="Akuntan">Akuntan</option>
													<option value="Konsultan">Konsultan</option>
													<option value="Dokter">Dokter</option>
													<option value="Bidan">Bidan</option>
													<option value="Perawat">Perawat</option>
													<option value="Apoteker">Apoteker</option>
													<option value="Psikiater/Psikolog">Psikiater/Psikolog</option>
													<option value="Penyiar Televisi">Penyiar Televisi</option>
													<option value="Penyiar Radio">Penyiar Radio</option>
													<option value="Pelaut">Pelaut</option>
													<option value="Peneliti">Peneliti</option>
													<option value="Sopir">Sopir</option>
													<option value="Pialang">Pialang</option>
													<option value="Pedagang">Pedagang</option>
													<option value="Perangkat Desa">Perangkat Desa</option>
													<option value="Kepala desa">Kepala desa</option>
													<option value="Biarawati">Biarawati</option>
													<option value="Wiraswasta">Wiraswasta</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputPenghasilan'>Penghasilan</label>
											<div class="form-line">
												<input type="text" class="form-control" name="penghasilan_edit" placeholder="Penghasilan" id="penghasilan_edit" />
											</div>
										</div>
									</div>
									<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
										<div class="form-group">
											<label for='InputStatusDalam Keluarga'>Status Dalam Keluarga</label>
											<div class="form-line">
												<select class="form-control show-tick" name="sDalamKeluarga_edit" id="sDalamKeluarga_edit">
													<option value="">--- Pilih ---</option>
													<option value="Kepala Keluarga">Kepala keluarga</option>
													<option value="Suami">Suami</option>
													<option value="Istri">Istri</option>
													<option value="Anak">Anak</option>
													<option value="Menantu">Menantu</option>
													<option value="Cucu">Cucu</option>
													<option value="Orang Tua">Orang Tua</option>
													<option value="Mertua">Mertua</option>
													<option value="Famili Lain">Famili Lain</option>
													<option value="Pembantu">Pembantu</option>
													<option value="Lainnya">Lainnya</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputStatusPerkawinan'>Status Pernikahan</label>
											<div class="form-line">
												<select class="form-control show-tick" name="sPerkawinan_edit" id="sPerkawinan_edit">
													<option value="">--- Pilih ---</option>
													<option value="Belum Kawin">Belum Kawin</option>
													<option value="Kawin">Kawin</option>
													<option value="Cerai">Cerai</option>													
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAkteKawin'>Akte Pernikahan</label>
											<div class="form-line">
												<select id="aktekawin_edit" class="form-control show-tick" name="aktekawin_edit">
													<option value="">---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fix_no_aktekawin_edit">
										<div class="form-group">
											<label for='InputNoAkteKawin'>No Akte Pernikahan</label>
											<div class="form-line">
												<input type="text" class="form-control" name="no_aktekawin_edit" placeholder="No Akte Perkawinan" id="no_aktekawin_edit" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fix_tgl_aktekawin_edit">
										<div class="form-group">
											<label for='InputTglKawin'>Tanggal Pernikahan</label>
											<div class="form-line">
												<input type="text" class="form-control datepicker" name="tgl_aktekawin_edit" placeholder="Tanggal Perkawinan"
												 id="tgl_aktekawin_edit" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAkteCerai'>Akte Cerai</label>
											<div class="form-line">
												<select id="aktecerai_edit" class="form-control show-tick" name="aktecerai_edit">
													<option value="">---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fix_no_aktecerai_edit">
										<div class="form-group">
											<label for='InputNoAkteCerai'>No Akte Perceraian</label>
											<div class="form-line">
												<input type="text" class="form-control" name="no_aktecerai_edit" placeholder="No Akte Perceraian" id="no_aktecerai_edit" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="fix_tgl_aktecerai_edit">
										<div class="form-group">
											<label for='InputTglCerai'>Tanggal Perceraian</label>
											<div class="form-line">
												<input type="text" class="form-control datepicker" name="tgl_aktecerai_edit" placeholder="Tanggal Percereian"
												 id="tgl_aktecerai_edit" />
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputAkteLahir'>Kelainan FM</label>
											<div class="form-line">
												<select id="kelainanFM_edit" class="form-control show-tick" name="kelainanFM_edit">
													<option>---Pilih---</option>
													<option value="Tidak Ada">Tidak Ada</option>
													<option value="Ada">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='inputStatusHidup'>Status Hidup</label>
											<div class="form-line">
												<select id="status_hidup_edit" class="form-control show-tick" name="status_hidup_edit">
													<option value="">---Pilih---</option>
													<option value="Hidup">Tidak Ada</option>
													<option value="Meninggal">Ada</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputNamaAyah'>Photo Foto Saat Ini</label>
											<div id="preview_photo"></div>
											<input type="checkbox" id="basic_checkbox_1" name="ganti_foto" class="chk-col-black" value="check_foto">
											<label for="basic_checkbox_1">Ganti Foto</label>
										</div>
									</div>

									<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
										<div class="form-group">
											<label for='InputNamaAyah'>Foto</label>
											<div class="form-line">
												<input type="file" class="form-control" name="image_edit" id="image_edit" />
											</div>
										</div>
									</div>
							</div>
							<div class="modal-footer">
								<input type="submit" class="btn btn-primary waves-effect" value="EDIT">
								<button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
							</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal fade" id="modal_detail_penduduk" tabindex="-1" role="dialog">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="defaultModalLabel">Detail Penduduk</h4>
							</div>
							<div class="modal-body">
								<div class="bs-example" data-example-id="media-alignment">
									<div class="media">
										<div class="media-left">
											<div id="detail_foto"></div>
										</div>
										<div class="media-body">
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='inputNIK'>NIK</label>
													<div class="form-line">
														<input type="text" class="form-control" name="nik_detail" id="nik_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='inputNamaLengkap'>Nama Lengkap</label>
													<div class="form-line">
														<input type="text" class="form-control" name="nama_detail" id="nama_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='InputAlamat'>Alamat</label>
													<div class="form-line">
														<textarea rows="1" class="form-control no-resize auto-growth" name="alamat_detail" id="alamat_detail"
														 disabled></textarea>
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='inputTTL'>Tempat, Tanggal Lahir</label>
													<div class="form-line">
														<input type="text" class="form-control" name="ttl_detail" id="ttl_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='inputJenisKelamin'>Jenis Kelamin</label>
													<div class="form-line">
														<input type="text" class="form-control" name="jk_detail" id="jk_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='InputGolDar'>Golongan Darah</label>
													<div class="form-line">
														<input type="text" class="form-control" name="goldar_detail" id="goldar_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='inputStatusPerkawinan'>Status Pernikahan</label>
													<div class="form-line">
														<input type="text" class="form-control" name="sPerkawinan_detail" id="sPerkawinan_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='agama'>Agama</label>
													<div class="form-line">
														<input type="text" class="form-control" name="agama_detail" id="agama_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='pekerjaan'>Pekerjaan</label>
													<div class="form-line">
														<input type="text" class="form-control" name="pekerjaan_detail" id="pekerjaan_detail" disabled />
													</div>
												</div>
											</div>
											<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
												<div class="form-group">
													<label for='penghasilan'>Penghasilan</label>
													<div class="form-line">
														<input type="text" class="form-control" name="penghasilan_detail" id="penghasilan_detail" disabled />
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- #END# Task Info -->
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
					<div class="card">
						<div class="body bg-pink">
							<div class="font-bold m-b--35">Informasi</div>
							<ul class="dashboard-stat-list">
								<li>
									Data Penduduk Desa harus valid. Silahkan lengkapi data disamping dengan data sebenarnya.
								</li>
							</ul>
						</div>
					</div>
				</div>
	</section>

	<?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>