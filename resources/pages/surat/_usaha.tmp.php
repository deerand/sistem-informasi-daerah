<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
      </div>
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="header">
              <h2>Surat Keterangan Usaha</h2>
              <button type="button" class="btn btn-primary waves-effect" id="buatSurat">Buat
                Surat</button>
            </div>
            <div class="body">
              <div id="formSuratUsaha" style="display:none;">
                <form method="POST" autocomplete="off" id="form_add_surat_usaha">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='inputNIK'>No Surat</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="no_surat" placeholder="No Surat" id="no_surat" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Pemohon</label>
                      <div class="form-line">
                        <input type="text" name="nik_pemohon" class="form-control cari_nik_sketusaha" placeholder="NIK Pemohon">
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed togglePemohon" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">Nama</td>
                          <td>:</td>
                          <td class="detail_nama"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td class="detail_jk"></td>
                        </tr>
                        <tr>
                          <td width="50">Alamat</td>
                          <td>:</td>
                          <td class="detail_tempat"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <input type="submit" class="btn btn-primary waves-effect" value="Tambah">
                  <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </form>
              </div>
              <div id="formEditSuratUsaha" style="display:none;">
                <form method="POST" autocomplete="off" id="form_edit_surat_usaha">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='inputNIK'>No Surat</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="no_surat_edit" placeholder="No Surat" id="no_surat_edit" />
                        <input type="hidden" class="form-control" name="id_edit" id="id_edit">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Pemohon</label>
                      <div class="form-line">
                        <input type="text" name="nik_pemohon_edit" class="form-control cari_nik_sketusaha_edit"
                          placeholder="NIK Pemohon">
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed togglePemohon_edit" style="display:none;">
                    <tbody>
                        <tr>
                          <td width="50">Nama</td>
                          <td>:</td>
                          <td class="detail_nama_edit"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td class="detail_jk_edit"></td>
                        </tr>
                        <tr>
                          <td width="50">Alamat</td>
                          <td>:</td>
                          <td class="detail_tempat_edit"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <input type="submit" class="btn btn-primary waves-effect" value="Edit">
                  <button type="button" class="btn btn-danger waves-effect">CLOSE</button>
                </form>
              </div>
            </div>
            <div class="table-responsive">
              <table class="table table-hover dashboard-task-infos" id="datatable_surat_usaha">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>No Surat</th>
                    <th>Nama</th>                    
                    <th>Jenis Kelamin</th>
                    <th>Nama Admin</th>
                    <th>Tanggal Buat</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #END# Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="body bg-pink">
              <div class="font-bold m-b--35">Informasi</div>
              <ul class="dashboard-stat-list">
                <li>
                  Data harus valid. Silahkan lengkapi data disamping dengan data sebenarnya.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>