<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>

  <section class="content">
    <div class="container-fluid">
      <div class="block-header">
      </div>
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
          <div class="card">
            <div class="header">
              <h2>Surat Keterangan Kelahiran</h2>
              <button type="button" class="btn btn-primary waves-effect" id="buatSurat">Buat
                Surat</button>
            </div>
            <div class="body">
              <div id="formSuratKelahiran" style="display:none;">
                <form method="POST" autocomplete="off" id="form_add_surat_kelahiran">
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='inputNIK'>No Surat</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="no_surat" placeholder="No Surat" id="no_surat" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputNama'>Nama Lengkap</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="nama_anak" placeholder="Nama Lengkap" id="nama_anak" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputJK'>Jenis Kelamin</label>
                      <div class="form-line">
                        <select id="jk" class="form-control show-tick" name="jk">
                          <option value="">---Pilih---</option>
                          <option value="P">Pria</option>
                          <option value="W">Wanita</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputNama'>Tempat Lahir</label>
                      <div class="form-line">
                        <input type="text" class="form-control" name="tmp_lahir" placeholder="Tempat Lahir" id="tmp_lahir" />
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>Tanggal Lahir</label>
                      <div class="form-line">
                        <input type="text" name="tgl_lahir" id="tgl_lahir" class="form-control datepicker" placeholder="Tanggal Lahir">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputAgama'>Agama</label>
                      <div class="form-line">
                        <select class="form-control show-tick" name="agama" id="agama" required>
                          <option value="">--- Pilih ---</option>
                          <option value="Islam">Islam</option>
                          <option value="Kristen">Kristen</option>
                          <option value="Katolik">Katolik</option>
                          <option value="Hindu">Hindu</option>
                          <option value="Budha">Budha</option>
                          <option value="Khonghucu">Khonghucu</option>
                          <option value="Kepercayaan">Kepercayaan</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputAlamat'>Alamat</label>
                      <div class="form-line">
                        <textarea rows="1" class="form-control no-resize auto-growth" name="alamat" placeholder="Alamat"
                          id="alamat"></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>Anak Ke-</label>
                      <div class="form-line">
                        <input type="number" name="anakke" id="anakke" class="form-control" placeholder="Anak Ke-">
                      </div>
                    </div>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Ayah</label>
                      <div class="form-line">
                        <input type="text" name="nik_ayah" id="nik_ayah" class="form-control" placeholder="NIK Ayah">
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed" id="toggleAyah" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">Nama</td>
                          <td>:</td>
                          <td id="nama_ayah"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td id="jk_ayah"></td>
                        </tr>
                        <tr>
                          <td width="50">Tempat Tinggal</td>
                          <td>:</td>
                          <td id="alamat_ayah"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                      <label for='InputTglLahir'>NIK Ibu</label>
                      <div class="form-line">
                        <input type="text" name="nik_ibu" id="nik_ibu" class="form-control" placeholder="NIK Ibu">
                      </div>
                    </div>
                    <table class="table table-responsive table-hover table-condensed" id="toggleIbu" style="display:none;">
                      <tbody>
                        <tr>
                          <td width="50">Nama</td>
                          <td>:</td>
                          <td id="nama_ibu"></td>
                        </tr>
                        <tr>
                          <td width="50">Jenis Kelamin</td>
                          <td>:</td>
                          <td id="jk_ibu"></td>
                        </tr>
                        <tr>
                          <td width="50">Tempat Tinggal</td>
                          <td>:</td>
                          <td id="alamat_ibu"></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <input type="submit" class="btn btn-primary waves-effect" value="Tambah">
                  <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                </form>
              </div>
              <div id="formEditSuratKelahiran" style="display:none;">
                  <form method="POST" autocomplete="off" id="form_edit_surat_kelahiran">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='inputNoSurat'>No Surat</label>
                        <div class="form-line">
                          <input type="text" class="form-control" name="no_surat_edit" placeholder="No Surat" id="no_surat_edit" />
                          <input type="hidden" name="id_edit" id="id_edit">
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputNama'>Nama Lengkap</label>
                        <div class="form-line">
                          <input type="text" class="form-control" name="nama_anak_edit" placeholder="Nama Lengkap" id="nama_anak_edit" />
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputJK'>Jenis Kelamin</label>
                        <div class="form-line">
                          <select id="jk_edit" class="form-control show-tick" name="jk_edit">
                            <option value="">---Pilih---</option>
                            <option value="P">Pria</option>
                            <option value="W">Wanita</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputNama'>Tempat Lahir</label>
                        <div class="form-line">
                          <input type="text" class="form-control" name="tmp_lahir_edit" placeholder="Tempat Lahir" id="tmp_lahir_edit" />
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputTglLahir'>Tanggal Lahir</label>
                        <div class="form-line">
                          <input type="text" name="tgl_lahir_edit" id="tgl_lahir_edit" class="form-control datepicker" placeholder="Tanggal Lahir">
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputAgama'>Agama</label>
                        <div class="form-line">
                          <select class="form-control show-tick" name="agama_edit" id="agama_edit" required>
                            <option value="">--- Pilih ---</option>
                            <option value="Islam">Islam</option>
                            <option value="Kristen">Kristen</option>
                            <option value="Katolik">Katolik</option>
                            <option value="Hindu">Hindu</option>
                            <option value="Budha">Budha</option>
                            <option value="Khonghucu">Khonghucu</option>
                            <option value="Kepercayaan">Kepercayaan</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputAlamat'>Alamat</label>
                        <div class="form-line">
                          <textarea rows="1" class="form-control no-resize auto-growth" name="alamat_edit" placeholder="Alamat"
                            id="alamat_edit"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputTglLahir'>Anak Ke-</label>
                        <div class="form-line">
                          <input type="number" name="anakke_edit" id="anakke_edit" class="form-control" placeholder="Anak Ke-">
                        </div>
                      </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputTglLahir'>NIK Ayah</label>
                        <div class="form-line">
                          <input type="text" name="nik_ayah_edit" id="nik_ayah_edit" class="form-control" placeholder="NIK Ayah">
                        </div>
                      </div>
                      <table class="table table-responsive table-hover table-condensed" id="toggleAyah" style="display:none;">
                        <tbody>
                          <tr>
                            <td width="50">Nama</td>
                            <td>:</td>
                            <td id="nama_ayah"></td>
                          </tr>
                          <tr>
                            <td width="50">Jenis Kelamin</td>
                            <td>:</td>
                            <td id="jk_ayah"></td>
                          </tr>
                          <tr>
                            <td width="50">Tempat Tinggal</td>
                            <td>:</td>
                            <td id="alamat_ayah"></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <div class="form-group">
                        <label for='InputTglLahir'>NIK Ibu</label>
                        <div class="form-line">
                          <input type="text" name="nik_ibu_edit" id="nik_ibu_edit" class="form-control" placeholder="NIK Ibu">
                        </div>
                      </div>
                      <table class="table table-responsive table-hover table-condensed" id="toggleIbu" style="display:none;">
                        <tbody>
                          <tr>
                            <td width="50">Nama</td>
                            <td>:</td>
                            <td id="nama_ibu"></td>
                          </tr>
                          <tr>
                            <td width="50">Jenis Kelamin</td>
                            <td>:</td>
                            <td id="jk_ibu"></td>
                          </tr>
                          <tr>
                            <td width="50">Tempat Tinggal</td>
                            <td>:</td>
                            <td id="alamat_ibu"></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <input type="submit" class="btn btn-primary waves-effect" value="Edit">
                    <button type="button" class="btn btn-danger waves-effect" >CLOSE</button>
                  </form>
                </div>
            </div>
            <div class="table-responsive">
              <table class="table table-hover dashboard-task-infos" id="datatable_surat_kelahiran">
                <thead>
                  <tr>
                    <th>NO</th>
                    <th>No Surat</th>
                    <th>Nama Pegawai</th>
                    <th>Nama Anak</th>
                    <th>Jenis Kelamin</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- #END# Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
          <div class="card">
            <div class="body bg-pink">
              <div class="font-bold m-b--35">Informasi</div>
              <ul class="dashboard-stat-list">
                <li>
                  Data pegawai Desa harus valid. Silahkan lengkapi data disamping dengan data sebenarnya.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php include_once "../../../resources/src/_js.php"; ?>
</body>

</html>