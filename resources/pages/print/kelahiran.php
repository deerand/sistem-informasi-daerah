<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <?php include_once "../../../resources/src/_css.php"; ?>
  <style>
    .pull-right {
      float: right !important;
    }

    .separator {
      border-bottom: 2px solid #616161;
      margin: -0.5rem 0 1.5rem;
    }

    @media print {
      .separator {
        border-bottom: 2px solid #616161;
        margin: -0.5rem 0 1rem;
      }

    }
  </style>
</head>

<body onload="window.print()">
  <?php
    include_once "../../../backend/config/koneksi.php";
    $id = $_GET['id'];
    $query = mysqli_query($con,"SELECT skelahiran.*,pen1.nama AS nama_ayah,pen1.jk AS jk_ayah,pen1.tgl_lahir AS tgl_lahir_ayah, pen1.agama AS agama_ayah, pen1.alamat AS alamat_ayah, pen2.nama AS nama_ibu,pen2.nama AS nama_ibu,pen2.jk AS jk_ibu,pen2.tgl_lahir AS tgl_lahir_ibu, pen2.agama AS agama_ibu, pen2.alamat AS alamat_ibu FROM skelahiran INNER JOIN penduduk pen1 ON skelahiran.nik_ayah=pen1.nik INNER JOIN penduduk pen2 ON skelahiran.nik_ibu=pen2.nik WHERE skelahiran.id ='$id'");
    while($data = mysqli_fetch_array($query))
    {
      $no_surat = $data['no_surat'];
      $nama_anak = $data['nama_anak'];
      $jk = $data['jk'];
      $tmp_lahir = $data['tmp_lahir'];
      $tgl_lahir = $data['tgl_lahir'];
      $agama = $data['agama'];
      $alamat = $data['alamat'];
      $nama_ayah = $data['nama_ayah'];
      $jk_ayah = $data['jk_ayah'];
      $tgl_lahir_ayah = $data['tgl_lahir_ayah'];
      $agama_ayah = $data['agama_ayah'];
      $alamat_ayah = $data['alamat_ayah'];
      $nama_ibu = $data['nama_ibu'];
      $jk_ibu = $data['jk_ibu'];
      $tgl_lahir_ibu = $data['tgl_lahir_ibu'];
      $agama_ibu = $data['agama_ibu'];
      $alamat_ibu = $data['alamat_ibu'];
    }
  ?>
  <div class="container">
    <div class="row">    
      <div class="col-md-12">
        <img src="../../../images/logo/logo.jpeg" alt="" width=100 height=100 style="float:left;margin-top=10px;">
        <h4 class="text-center">PEMERINTAH KABUPATEN SUBANG</h4>
        <h5 class="text-center">KECAMATAN TAMBAKDAHAN</h5>
        <h6 class="text-center">KELURAHAN WANAJAYA</h6>
        <p class="text-center">Jl. Raya Wanajaya No.158, Ds.Wanajaya, Kec.Tambakdahan, Kab.Subang 41265 Jawa Barat</p>
      </div>
    </div>
    <div class="separator"></div>
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:16px;"><strong><u>Surat Kelahiran</u></strong></p>
        <p class="text-center" style="font-size:16px; margin-top: -15px">Nomer Surat: <?php echo $no_surat; ?></p>
        <p class="text-left">Menerangkan Dengan Sesungguhnya:</p>
        <div class="container">
          <table>
            <tbody>
              <tr>
                <td>1. </td>
                <td>Nama </td>
                <td>:
                  <?php echo $nama_anak;?>
                </td>
              </tr>
              <tr>
                <td>2. </td>
                <td>Jenis Kelamin </td>
                <td>:
                  <?php echo $jk;?>
                </td>
              </tr>
              <tr>
                <td>3. </td>
                <td>Tempat Kelahiran </td>
                <td>:
                  <?php echo $tmp_lahir;?>
                </td>
              </tr>
              <tr>
                <td>4. </td>
                <td>Tanggal Lahir </td>
                <td>:
                  <?php echo $tgl_lahir;?>
                </td>
              </tr>
              <tr>
                <td>5. </td>
                <td>Agama </td>
                <td>:
                  <?php echo $agama;?>
                </td>
              </tr>
              <tr>
                <td>6. </td>
                <td>Alamat </td>
                <td>:
                  <?php echo $alamat;?>
                </td>
              </tr>
            </tbody>
          </table>
          <p>Adalah benar Anak kandung yang ke 2 dari suami istri :</p>
          <p>I. Ayah</p>
          <div class="container">
            <table>
              <tbody>
                <tr>
                  <td>1. </td>
                  <td>Nama </td>
                  <td>:
                    <?php echo $nama_ayah;?>
                  </td>
                </tr>
                <tr>
                  <td>2. </td>
                  <td>Jenis Kelamin </td>
                  <td>:
                    <?php echo $jk_ayah;?>
                  </td>
                </tr>
                <tr>
                  <td>3. </td>
                  <td>Tanggal Lahir </td>
                  <td>:
                    <?php echo $tgl_lahir_ayah;?>
                  </td>
                </tr>
                <tr>
                  <td>4. </td>
                  <td>Agama </td>
                  <td>:
                    <?php echo $agama_ayah;?>
                  </td>
                </tr>
                <tr>
                  <td>5. </td>
                  <td>Alamat </td>
                  <td>:
                    <?php echo $alamat_ayah;?>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <p>II. Istri</p>
          <div class="container">
            <table>
              <tbody>
                <tr>
                  <td>1. </td>
                  <td>Nama </td>
                  <td>:
                    <?php echo $nama_ibu;?>
                  </td>
                </tr>
                <tr>
                  <td>2. </td>
                  <td>Jenis Kelamin </td>
                  <td>:
                    <?php echo $jk_ibu;?>
                  </td>
                </tr>
                <tr>
                  <td>3. </td>
                  <td>Tanggal Lahir </td>
                  <td>:
                    <?php echo $tgl_lahir_ibu;?>
                  </td>
                </tr>
                <tr>
                  <td>4. </td>
                  <td>Agama </td>
                  <td>:
                    <?php echo $agama_ibu;?>
                  </td>
                </tr>
                <tr>
                  <td>5. </td>
                  <td>Alamat </td>
                  <td>:
                    <?php echo $alamat_ibu;?>
                  </td>
                </tr>
              </tbody>
            </table>
            <p style="margin-top:10px;">Demikian surat keterangan kelahiran ini diberikan kepada yang bersangkutan
              untuk dapat</p>
          </div>
          <p>dipergunakan sebagaimana mestinya.</p>
        </div>
      </div>
    </div>
    <div class="pull-right">
      <table>
        <tr>
          <td>
            <p class="text-center">Kepala Desa</p>
          </td>
        </tr>
        <tr>
          <td rowspan="1">
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>

        <tr>
          <td>
            <p class="text-center"></p>
          </td>
        </tr>

        <tr>
          <td>
            <p class="text-center">H. Sakim</p>
          </td>
        </tr>
        <tr>
          <td>
            <p class="text-left" style="margin-top:-10px;">NIP.</p>
          </td>
        </tr>
      </table>
    </div>
  </div>
</body>

</html>