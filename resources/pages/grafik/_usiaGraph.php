<?php include_once "../../../utils/authenticated.php"; ?>
<!DOCTYPE html>
<html>

<head>
  <?php include_once "../../../resources/partrials/_header.php"; ?>
  <?php include_once "../../../resources/src/_css.php"; ?>
</head>

<body class="theme-red">
  <!-- Page Loader -->
  <?php include_once "../../../resources/partrials/_loader.php"; ?>
  <!-- #END# Page Loader -->
  <!-- Top Bar -->
  <?php include "../../../resources/partrials/_topbar.php"; ?>
  <!-- #Top Bar -->
  <section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
      <!-- User Info -->
      <?php include_once "../../../resources/partrials/_userinfo.php"; ?>
      <!-- #User Info -->
      <!-- Menu -->
      <?php include_once "../../../resources/partrials/_sidebar.php"; ?>
      <!-- #Menu -->
      <!-- Footer -->
      <?php include_once "../../../resources/partrials/_footer.php"; ?>
      <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
  </section>
  <section class="content">
    <div class="container-fluid">
      <div class="row clearfix">
        <!-- Task Info -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="card">
            <div class="header">
              <h2>Grafik Penduduk Berdasarkan Usia</h2>
            </div>
            <div class="body">
              <canvas id="usiaGraph"></canvas>
              <table class="table table-responsive table-hover" style="margin-top:50px">
                <thead>
                  <tr>
                    <td>No</td>
                    <td>Usia</td>
                    <td>Jumlah</td>
                    <td>Persentase</td>
                  </tr>
                </thead>
                <tbody id="persentaseUsia">
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php include_once "../../../resources/src/_js.php"; ?>
  <script src="http://localhost:8000/js/graph/_usiaGraph.js"></script>
</body>

</html>