  $(document).ready(function () {

    var url = window.location.href.split('/')[0] + '/';

    $("#aktelahir").change(function () {
      if ($(this).val() == "Tidak Ada" || $(this).val() == "") {
        $('#fix_no_akte_lahir').hide().css({
          "display": "none"
        });
      } else {
        $('#fix_no_akte_lahir').show().css({
          "display": "inline"
        });
      }
    });
    $("#jenis_mutasi").change(function () {
      if ($(this).val() == "Keluar") {
        $('#mutasiKeluar').show('slow');
        $('#mutasiMasuk').hide('slow');
        $('#nama_lengkap').val('');
        $('#tgl_lahir').val('');
      } else if ($(this).val() == "Masuk") {
        $('#mutasiMasuk').show('slow');
        $('#mutasiKeluar').hide('slow');
        $('#cari_nik').val('');
      } else {
        return false;
      }
    });
    $("#aktelahir_edit").change(function () {
      if ($(this).val() == "Tidak Ada" || $(this).val() == "") {
        $('#fix_no_aktelahir_edit').hide().css({
          "display": "none"
        });
      } else {
        $('#fix_no_aktelahir_edit').show().css({
          "display": "inline"
        });
      }
    });

    $("#aktekawin").change(function () {
      if ($(this).val() == "Ada") {
        $('#fix_no_aktekawin').show().css({
          "display": "inline"
        });
        $('#fix_tgl_aktekawin').show().css({
          "display": "inline"
        });
      } else {
        $('#fix_no_aktekawin').hide().css({
          "display": "none"
        });
        $('#fix_tgl_aktekawin').hide().css({
          "display": "none"
        });
      }
    })
    $("#aktekawin_edit").change(function () {
      if ($(this).val() == "Ada") {
        $('#fix_no_aktekawin_edit').show().css({
          "display": "inline"
        });
        $('#fix_tgl_aktekawin_edit').show().css({
          "display": "inline"
        });
      } else {
        $('#fix_no_aktekawin_edit').hide().css({
          "display": "none"
        });
        $('#fix_tgl_aktekawin_edit').hide().css({
          "display": "none"
        });
      }
    })

    $("#aktecerai").change(function () {
      if ($(this).val() == "Ada") {
        $('#fix_no_aktecerai').show().css({
          "display": "inline"
        });
        $('#fix_tgl_aktecerai').show().css({
          "display": "inline"
        });
      } else {
        $('#fix_no_aktecerai').hide().css({
          "display": "none"
        });
        $('#fix_tgl_aktecerai').hide().css({
          "display": "none"
        });
      }
    })
    $("#aktecerai_edit").change(function () {
      if ($(this).val() == "Ada") {
        $('#fix_no_aktecerai_edit').show().css({
          "display": "inline"
        });
        $('#fix_tglcerai_edit').show().css({
          "display": "inline"
        });
      } else {
        $('#fix_no_aktecerai_edit').hide().css({
          "display": "none"
        });
        $('#fix_tgl_aktecerai_edit').hide().css({
          "display": "none"
        });
      }
    })
    $('#datatable_data_penduduk').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": [0],
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/penduduk/lihat_penduduk.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "nik"
        },
        {
          "data": "nama"
        },
        {
          "data": "jk"
        },
        {
          "data": "tmp_lahir"
        },
        {
          "data": "tgl_lahir"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
          return `<button id="${data}">Data</button>`;
          }
        },
      ],
      "order": [
        [6, 'asc']
      ]
    });
    $('#datatable_mutasi').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/mutasi/lihat_mutasi.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "no_surat"
        },
        {
          "data": "jenis_mutasi"
        },
        {
          "data": "kategori_kepindahan"
        },
        {
          "data": "nik_pemohon"
        },
      ],
      "order": [
        [1, 'asc']
      ]
    });

    $('#datatable_data_penduduk_kurang_mampu').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/penduduk/lihat_penduduk_kurang_mampu.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "nik"
        },
        {
          "data": "nama"
        },
        {
          "data": "jk"
        },
        {
          "data": "tmp_lahir"
        },
        {
          "data": "tgl_lahir"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
          <button class=\"btn btn-info waves-effect\" id=\"edit_penduduk\" data-id_penduduk=\"" + data + "\">Edit</button>\
          <button class=\"btn btn-success waves-effect\" id=\"detail_penduduk\" data-id_penduduk=\"" + data + "\">Detail</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });
    $('#datatable_data_penduduk_kepala_keluarga').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/penduduk/lihat_penduduk_kepala_keluarga.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "nik"
        },
        {
          "data": "nama"
        },
        {
          "data": "jk"
        },
        {
          "data": "tmp_lahir"
        },
        {
          "data": "tgl_lahir"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
          <button class=\"btn btn-info waves-effect\" id=\"edit_penduduk\" data-id_penduduk=\"" + data + "\">Edit</button>\
          <button class=\"btn btn-success waves-effect\" id=\"detail_penduduk\" data-id_penduduk=\"" + data + "\">Detail</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });
    $('#datatable_data_penduduk_meninggal').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/penduduk/lihat_penduduk_meninggal.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "nik"
        },
        {
          "data": "nama"
        },
        {
          "data": "jk"
        },
        {
          "data": "tmp_lahir"
        },
        {
          "data": "tgl_lahir"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
          <button class=\"btn btn-success waves-effect\" id=\"detail_penduduk\" data-id_penduduk=\"" + data + "\">Detail</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });
    $('#datatable_surat_kelahiran').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/sKelahiran/lihat_surat_kelahiran.php',
      "columns": [{
          "data": "no"
        },

        {
          "data": "no_surat"
        },

        {
          "data": "nama"
        },

        {
          "data": "nama_anak"
        },

        {
          "data": "jk"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
          <button class=\"btn btn-danger waves-effect\" id=\"delete_skelahiran\" data-id_surat=\"" + data + "\">Delete</button>\
          <button class=\"btn btn-info waves-effect\" id=\"edit_skelahiran\" data-id_surat=\"" + data + "\">Edit</button>\
          <button class=\"btn btn-success waves-effect\" id=\"print_skelahiran\" data-id_surat=\"" + data + "\">Print</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });
    $('#datatable_surat_meninggal').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/sMeninggal/lihat_surat_meninggal.php',
      "columns": [{
          "data": "no"
        },

        {
          "data": "no_surat"
        },
        {
          "data": "nama"
        },
        {
          "data": "tgl_meninggal"
        },
        {
          "data": "jk"
        },
        {
          "data": "nama_pegawai"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "<button class=\"btn btn-success waves-effect\" id=\"print_meninggal\" data-print_id_meninggal=\"" + data + "\">Print</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });
    $('#datatable_surat_usaha').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/sUsaha/lihat_surat_usaha.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "no_surat"
        },
        {
          "data": "nama"
        },
        {
          "data": "jk"
        },
        {
          "data": "nama_pegawai"
        },
        {
          "data": "created_at"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
              <button class=\"btn btn-info waves-effect\" id=\"edit_sketusaha\" data-id_edit_usaha=\"" + data + "\">Edit</button>\
              <button class=\"btn btn-danger waves-effect\" id=\"delete_sketusaha\" data-id_usaha=\"" + data + "\">Delete</button>\
              <button class=\"btn btn-success waves-effect\" id=\"print_sketusaha\" data-id_usaha_print=\"" + data + "\">Print</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });

    $('#datatable_surat_belum_nikah').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/sBelumNikah/lihat_surat_belum_nikah.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "no_surat"
        },
        {
          "data": "nama"
        },
        {
          "data": "jk"
        },
        {
          "data": "sperkawinan"
        },
        {
          "data": "nama_pegawai"
        },
        {
          "data": "created_at"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
              <button class=\"btn btn-info waves-effect\" id=\"edit_belum_nikah\" data-id_edit_belum_nikah=\"" + data + "\">Edit</button>\
              <button class=\"btn btn-danger waves-effect\" id=\"delete_belum_nikah\" data-id_belum_nikah=\"" + data + "\">Delete</button>\
              <button class=\"btn btn-success waves-effect\" id=\"print_belum_nikah\" data-id_print_belum_nikah=\"" + data + "\">Print</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });

    $('#datatable_surat_cerai').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/sCerai/lihat_surat_cerai.php',
      "columns": [{
          "data": "no"
        },

        {
          "data": "no_surat"
        },
        {
          "data": "nik_pemohon"
        },
        {
          "data": "nik_pasangan"
        },
        {
          "data": "nama"
        },
        {
          "data": "tgl_cerai"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
          <button class=\"btn btn-info waves-effect\" id=\"edit_smeninggal\" data-id_smeninggal=\"" + data + "\">Edit</button>";
          }
        },

      ],
      "order": [
        [1, 'asc']
      ]
    });
    /*
    $("#datatable_data_penduduk").on('click', '#delete_smeninggal', function (e) {
      swal({
        title: "Apakah yakin? ",
        text: "Data telah terhapus, kamu tidak akan bisa untuk mengembalikan data yang telah di hapus!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          const id_smeninggal = $(this).data('id_smeninggal');

          $.ajax({
            url: url + 'backend/penduduk/delete_smeninggal.php',
            type: 'POST',
            data: {
              id_smeninggal: id_smeninggal
            },
            dataType: 'JSON',

            success: function (data) {
              if (data.status == "400") {
                swal(data.response, {
                  icon: "error",
                });
              } else {
                swal(data.response, {
                  icon: "success",
                });
                $('#datatable_surat_meninggal').DataTable().ajax.reload(null, false).draw();
              }
            },

          })
        } else {
          swal("Data Tidak Jadi di Hapus");
        }
      });    
    });
    */
    $("#datatable_surat_usaha").on('click', '#edit_sketusaha', function (e) {
      const id_edit_usaha = $(this).data('id_edit_usaha');  
      
      $.ajax({
        url: url + 'backend/sUsaha/edit_usaha.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_edit_usaha: id_edit_usaha
        },
        success: function (data) {
          console.log(data);
          $('#formEditSuratUsaha').show('slow');
          $('#id_edit').val(data.id);
          $('#no_surat_edit').val(data.no_surat);
          $('.cari_nik_sketusaha_edit').val(data.nik_pemohon);
          $('.togglePemohon_edit').show('slow');
          $('.detail_nama_edit').html(data.nama)
          $('.detail_jk_edit').html(data.jk);
          $('.detail_tempat_edit').html(data.alamat);
        }
      });
    });

    $("#datatable_surat_belum_nikah").on('click', '#edit_belum_nikah', function (e) {
      const id_edit_belum_nikah = $(this).data('id_edit_belum_nikah');  
      
      $.ajax({
        url: url + 'backend/sBelumNikah/edit_belum_nikah.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_edit_belum_nikah: id_edit_belum_nikah
        },
        success: function (data) {
          console.log(data);
          $('#formEditSuratBelumNikah').show('slow');
          $('#id_edit').val(data.id);
          $('#no_surat_edit').val(data.no_surat);
          $('.cari_nik_belum_nikah_edit').val(data.nik_pemohon);
          $('.togglePemohon_edit').show('slow');
          $('.detail_nama_edit').html(data.nama)
          $('.detail_jk_edit').html(data.jk);
          $('.detail_tempat_edit').html(data.alamat);
          $('.detail_sPerkawinan_edit').html(data.sperkawinan);
        }
      });
    });

    $("#datatable_surat_usaha").on('click', '#delete_sketusaha', function (e) {
      swal({
          title: "Apakah yakin? ",
          text: "Data telah terhapus, kamu tidak akan bisa untuk mengembalikan data yang telah di hapus!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            const id_usaha = $(this).data('id_usaha');

            $.ajax({
              url: url + 'backend/sUsaha/delete_usaha.php',
              type: 'POST',
              data: {
                id_usaha: id_usaha
              },
              dataType: 'JSON',
              success: function (data) {
                if (data.status == "400") {
                  swal(data.response, {
                    icon: "error",
                  });
                } else {
                  swal(data.response, {
                    icon: "success",
                  });
                  $('#datatable_surat_usaha').DataTable().ajax.reload(null, false).draw();
                }
              },

            })
          } else {
            swal("Data Tidak Jadi di Hapus");
          }
        });
    });
    $("#datatable_surat_belum_nikah").on('click', '#delete_belum_nikah', function (e) {
      swal({
          title: "Apakah yakin? ",
          text: "Data telah terhapus, kamu tidak akan bisa untuk mengembalikan data yang telah di hapus!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            const id_belum_nikah = $(this).data('id_belum_nikah');

            $.ajax({
              url: url + 'backend/sBelumNikah/delete_belum_nikah.php',
              type: 'POST',
              data: {
                id_belum_nikah: id_belum_nikah
              },
              dataType: 'JSON',
              success: function (data) {
                if (data.status == "400") {
                  swal(data.response, {
                    icon: "error",
                  });
                } else {
                  swal(data.response, {
                    icon: "success",
                  });
                  $('#datatable_surat_belum_nikah').DataTable().ajax.reload(null, false).draw();
                }
              },

            })
          } else {
            swal("Data Tidak Jadi di Hapus");
          }
        });
    });
    $("#datatable_data_penduduk").on('click', '#delete_penduduk', function (e) {
      swal({
          title: "Apakah yakin? ",
          text: "Data telah terhapus, kamu tidak akan bisa untuk mengembalikan data yang telah di hapus!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            const id_penduduk = $(this).data('id_penduduk');

            $.ajax({
              url: url + 'backend/penduduk/delete_penduduk.php',
              type: 'POST',
              data: {
                id_penduduk: id_penduduk
              },
              dataType: 'JSON',

              success: function (data) {
                if (data.status == "400") {
                  swal(data.response, {
                    icon: "error",
                  });
                } else {
                  swal(data.response, {
                    icon: "success",
                  });
                  $('#datatable_data_penduduk').DataTable().ajax.reload(null, false).draw();
                }
              },

            })
          } else {
            swal("Data Tidak Jadi di Hapus");
          }
        });
    })
    $("#form_add_penduduk").validate({
      rules: {
        nik: {
          remote: url + 'backend/validate/nik_penduduk.php',
        },
      },
      messages: {
        nik: {
          remote: "Nik Sudah Terpakai"
        },
      },
      highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      submitHandler: function () {
        $("#form_add_penduduk").submit(function (e) {
          e.preventDefault();
          $.ajax({
            type: 'POST',
            url: url + 'backend/penduduk/tambah_penduduk.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'JSON',
            success: function (data) {
              if (data.status == "400") {
                swal({
                  title: "Cek!",
                  text: "Cek Kondisi Form!",
                  icon: "warning",
                });
              } else {
                $("#form_add_penduduk")[0].reset();
                $('#jk').selectpicker('val', '');
                $('#agama').selectpicker('val', '');
                $('#aktelahir').selectpicker('val', '');
                $('#no_aktelahir').selectpicker('val', '');
                $('#pendidikan').selectpicker('val', '');
                $('#pekerjaan').selectpicker('val', '');
                $('#sDalamKeluarga').selectpicker('val', '');
                $('#sPerkawinan').selectpicker('val', '');
                $('#aktekawin').selectpicker('val', '');
                $('#no_aktekawin').selectpicker('val', '');
                $('#tgl_no_aktekawin').selectpicker('val', '');
                $('#aktecerai').selectpicker('val', '');
                $('#no_aktecerai').selectpicker('val', '');
                $('#tgl_aktecerai').selectpicker('val', '');
                $('#goldar').selectpicker('val', '');
                $('#kelainanFM').selectpicker('val', '');
                $('#status_hidup').selectpicker('val', '');

                $('#modal_add_penduduk').modal('hide');

                $('#datatable_data_penduduk').DataTable().ajax.reload(null, false).draw();

                swal({
                  title: "Berhasil!",
                  text: "Kamu telah berhasil menambahkan data!",
                  icon: "success",
                });
              }
            },
          });
        });
      }
    })
    $("#datatable_surat_meninggal").on('click', '#print_meninggal', function (e) {
      const id = $(this).data('print_id_meninggal');
      let page = url + 'resources/pages/print/meninggal.php?id=' + id
      var newWindow = window.open("", "_blank");
      newWindow.location.href = page;
      
    });    
    $("#datatable_surat_usaha").on('click', '#print_sketusaha', function (e) {
      const id = $(this).data('id_usaha_print');
      let page = url + 'resources/pages/print/ketusaha.php?id=' + id
      var newWindow = window.open("", "_blank");
      newWindow.location.href = page;
    });
    $("#datatable_surat_belum_nikah").on('click', '#print_belum_nikah', function (e) {
      const id = $(this).data('id_print_belum_nikah');
      let page = url + 'resources/pages/print/ketblmnikah.php?id=' + id
      var newWindow = window.open("", "_blank");
      newWindow.location.href = page;
    });
    $("#datatable_surat_kelahiran").on('click', '#print_skelahiran', function (e) {
      const id = $(this).data('id_surat');
      let page = url + 'resources/pages/print/kelahiran.php?id=' + id
      var newWindow = window.open("", "_blank");
      newWindow.location.href = page;
    });
    $("#datatable_data_penduduk").on('click', '#edit_penduduk', function (e) {
      const id_penduduk = $(this).data('id_penduduk');
      $('#modal_edit_penduduk').modal('show');
      $.ajax({
        url: url + 'backend/penduduk/edit_penduduk.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_penduduk: id_penduduk
        },
        success: function (data) {
          if (data.aktelahir == "Tidak Ada") {
            $('#fix_no_aktelahir_edit').hide().css({
              "display": "none"
            })
          }
          if (data.aktekawin == "Tidak Ada") {
            $('#fix_no_aktekawin_edit').hide().css({
              "display": "none"
            })
            $('#fix_tgl_aktekawin_edit').hide().css({
              "display": "none"
            })
          }
          if (data.aktecerai == "Tidak Ada") {
            $('#fix_no_aktecerai_edit').hide().css({
              "display": "none"
            })
            $('#fix_tgl_aktecerai_edit').hide().css({
              "display": "none"
            })
          }
          $('#id_edit').val(data.id)
          $('#nik_edit').val(data.nik)
          $('#nama_edit').val(data.nama)
          $('#alamat_edit').val(data.alamat)
          $('#jk_edit').selectpicker('val', data.jk);
          $('#tmp_lahir_edit').val(data.tmp_lahir)
          $('#tgl_lahir_edit').val(data.tgl_lahir)
          $('#agama_edit').selectpicker('val', data.agama);
          $('#aktelahir_edit').selectpicker('val', data.aktelahir);
          $('#no_aktelahir_edit').val(data.no_aktelahir);
          $('#pendidikan_edit').selectpicker('val', data.pendidikan);
          $('#pekerjaan_edit').selectpicker('val', data.pekerjaan);
          $('#penghasilan_edit').val(data.penghasilan)
          $('#sDalamKeluarga_edit').selectpicker('val', data.sDalamKeluarga);
          $('#sPerkawinan_edit').selectpicker('val', data.sPerkawinan);
          $('#aktekawin_edit').selectpicker('val', data.aktekawin);
          $('#no_aktekawin_edit').val(data.no_aktekawin);
          $('#tgl_aktekawin_edit').val(data.tgl_aktekawin);
          $('#aktecerai_edit').selectpicker('val', data.aktecerai);
          $('#no_aktecerai_edit').val(data.no_aktecerai);
          $('#tgl_aktecerai_edit').val(data.tgl_aktecerai);
          $('#goldar_edit').selectpicker('val', data.goldar);
          $('#kelainanFM_edit').selectpicker('val', data.kelainanFM);
          $('#status_hidup_edit').selectpicker('val', data.status_hidup);
          $('#preview_photo').html('<img src="../../../images/penduduk/' + data.foto + '" height="200" width="200">')

        }
      })
    })
    $("#datatable_data_penduduk_kurang_mampu").on('click', '#edit_penduduk', function (e) {
      const id_penduduk = $(this).data('id_penduduk');
      $('#modal_edit_penduduk').modal('show');
      $.ajax({
        url: url + 'backend/penduduk/edit_penduduk.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_penduduk: id_penduduk
        },
        success: function (data) {
          if (data.aktelahir == "Tidak Ada") {
            $('#fix_no_aktelahir_edit').hide().css({
              "display": "none"
            })
          } else if (data.aktekawin == "Tidak Ada") {
            $('#fix_no_aktekawin_edit').hide().css({
              "display": "none"
            })
            $('#fix_tgl_aktekawin_edit').hide().css({
              "display": "none"
            })
          } else if (data.aktecerai == "Tidak Ada") {
            $('#fix_no_aktecerai_edit').hide().css({
              "display": "none"
            })
            $('#fix_tgl_aktecerai_edit').hide().css({
              "display": "none"
            })
          }
          $('#id_edit').val(data.id)
          $('#nik_edit').val(data.nik)
          $('#nama_edit').val(data.nama)
          $('#alamat_edit').val(data.alamat)
          $('#jk_edit').selectpicker('val', data.jk);
          $('#tmp_lahir_edit').val(data.tmp_lahir)
          $('#tgl_lahir_edit').val(data.tgl_lahir)
          $('#agama_edit').selectpicker('val', data.agama);
          $('#aktelahir_edit').selectpicker('val', data.aktelahir);
          $('#no_aktelahir_edit').val(data.no_aktelahir);
          $('#pendidikan_edit').selectpicker('val', data.pendidikan);
          $('#pekerjaan_edit').selectpicker('val', data.pekerjaan);
          $('#penghasilan_edit').val(data.penghasilan)
          $('#sDalamKeluarga_edit').selectpicker('val', data.sDalamKeluarga);
          $('#sPerkawinan_edit').selectpicker('val', data.sPerkawinan);
          $('#aktekawin_edit').selectpicker('val', data.aktekawin);
          $('#no_aktekawin_edit').val(data.no_aktekawin);
          $('#tgl_aktekawin_edit').val(data.tgl_aktekawin);
          $('#aktecerai_edit').selectpicker('val', data.aktecerai);
          $('#no_aktecerai_edit').val(data.no_aktecerai);
          $('#tgl_aktecerai_edit').val(data.tgl_aktecerai);
          $('#goldar_edit').selectpicker('val', data.goldar);
          $('#kelainanFM_edit').selectpicker('val', data.kelainanFM);
          $('#status_hidup_edit').selectpicker('val', data.status_hidup);
          $('#preview_photo').html('<img src="../../../images/penduduk/' + data.foto + '" height="200" width="200">')

        }
      })
    })
    $("#form_edit_penduduk").validate({
      rules: {
        nik_edit: {
          remote: {
            url: url + 'backend/validate/nik_edit_penduduk.php',
            type: 'POST',
            data: {
              id_penduduk: function () {
                return $('#id_edit').val();
              },
            }
          }
        },
      },
      messages: {
        nik_edit: {
          remote: "Nik Sudah Terpakai"
        },
      },
      highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      submitHandler: function () {
        $("#form_edit_penduduk").submit(function (e) {
          e.preventDefault();
          $.ajax({
            type: 'POST',
            url: url + 'backend/penduduk/update_penduduk.php',
            data: new FormData(this),
            contentType: false,
            processData: false,
            dataType: 'JSON',
            success: function (data) {
              if (data.status == "200") {
                $("#form_add_penduduk")[0].reset();

                $('#modal_edit_penduduk').modal('hide');

                $('#datatable_data_penduduk').DataTable().ajax.reload(null, false).draw();

                swal({
                  title: "Berhasil!",
                  text: "Kamu telah berhasil mengupdate data!",
                  icon: "success",
                });
              } else {
                swal({
                  title: "Cek!",
                  text: "Cek Kondisi Form!",
                  icon: "warning",
                });
              }
            },
          });
        });
      }
    });
    $('#datatable_data_pegawai').DataTable({
      "columnDefs": [{
        "searchable": false,
        "orderable": false,
        "targets": 0,
      }],
      "language": {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ Entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai/Tidak ada data",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      "processing": true,
      "serverSide": true,
      "autoWidth": false,
      "ajax": url + 'backend/pegawai/lihat_pegawai.php',
      "columns": [{
          "data": "no"
        },
        {
          "data": "nama"
        },
        {
          "data": "nip"
        },
        {
          "data": "jabatan"
        },
        {
          data: 'id',
          name: 'id',
          render: function (data) {
            return "\
          <button class=\"btn btn-danger waves-effect\" id=\"delete_pegawai\" data-id_pegawai=\"" + data + "\">Delete</button>\
          <button class=\"btn btn-primary waves-effect\" id=\"edit_pegawai\" data-id_pegawai=\"" + data + "\">Edit</button>\
          <button class=\"btn btn-success waves-effect\" id=\"detail_pegawai\" data-id_pegawai=\"" + data + "\">Detail</button>";
          }
        },
      ],
      "order": [
        [4, 'asc']
      ]
    });
    $("#datatable_data_penduduk").on('click', '#detail_penduduk', function (e) {
      let id_penduduk = $(this).data('id_penduduk');
      $('#modal_detail_penduduk').modal('show');
      $.ajax({
        url: url + 'backend/penduduk/edit_penduduk.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_penduduk: id_penduduk
        },
        success: function (data) {
          $('#id_detail').val(data.id)
          $('#nik_detail').val(data.nik)
          $('#nama_detail').val(data.nama)
          $('#alamat_detail').val(data.alamat)
          $('#jk_detail').val(data.jk);
          $('#ttl_detail').val(data.tmp_lahir + ', ' + data.tgl_lahir);
          $('#agama_detail').val(data.agama);
          $('#aktelahir_detail').val(data.aktelahir);
          $('#pendidikan_detail').val(data.pendidikan);
          $('#pekerjaan_detail').val(data.pekerjaan);
          $('#penghasilan_detail').val(data.penghasilan)
          $('#sDalamKeluarga_detail').val(data.sDalamKeluarga);
          $('#sPerkawinan_detail').val(data.sPerkawinan);
          $('#aktekawin_detail').val(data.aktekawin);
          $('#aktecerai_detail').val(data.aktecerai);
          $('#goldar_detail').val(data.goldar);
          $('#kelainanFM_detail').val(data.kelainanFM);
          $('#nikibu_detail').val(data.nikibu)
          $('#namaibu_detail').val(data.namaibu)
          $('#nikayah_detail').val(data.nikayah)
          $('#namaayah_detail').val(data.namaayah)
          $('#detail_foto').html('<img src="../../../images/penduduk/' + data.foto + '" height="200" width="200">')
        }
      })
    })
    $("#datatable_data_penduduk_kurang_mampu").on('click', '#detail_penduduk', function (e) {
      let id_penduduk = $(this).data('id_penduduk');
      $('#modal_detail_penduduk').modal('show');
      $.ajax({
        url: url + 'backend/penduduk/edit_penduduk.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_penduduk: id_penduduk
        },
        success: function (data) {
          $('#id_detail').val(data.id)
          $('#nik_detail').val(data.nik)
          $('#nama_detail').val(data.nama)
          $('#alamat_detail').val(data.alamat)
          $('#jk_detail').val(data.jk);
          $('#ttl_detail').val(data.tmp_lahir + ', ' + data.tgl_lahir);
          $('#agama_detail').val(data.agama);
          $('#aktelahir_detail').val(data.aktelahir);
          $('#pendidikan_detail').val(data.pendidikan);
          $('#pekerjaan_detail').val(data.pekerjaan);
          $('#penghasilan_detail').val(data.penghasilan)
          $('#sDalamKeluarga_detail').val(data.sDalamKeluarga);
          $('#sPerkawinan_detail').val(data.sPerkawinan);
          $('#aktekawin_detail').val(data.aktekawin);
          $('#aktecerai_detail').val(data.aktecerai);
          $('#goldar_detail').val(data.goldar);
          $('#kelainanFM_detail').val(data.kelainanFM);
          $('#nikibu_detail').val(data.nikibu)
          $('#namaibu_detail').val(data.namaibu)
          $('#nikayah_detail').val(data.nikayah)
          $('#namaayah_detail').val(data.namaayah)
          $('#detail_foto').html('<img src="../../../images/penduduk/' + data.foto + '" height="200" width="200">')
        }
      })
    })
    $("#form_add_pegawai").validate({
      rules: {
        nik: {
          remote: url + 'backend/validate/nik_pegawai.php',
        },
      },
      messages: {
        nik: {
          remote: "Nik Sudah Terpakai"
        },
      },
      highlight: function (element) {
        $(element).closest('.form-group').addClass('has-error');
      },
      submitHandler: function () {
        $("#form_add_pegawai").submit(function (e) {
          e.preventDefault();
          $.ajax({
            type: 'POST',
            url: url + 'backend/pegawai/tambah_pegawai.php',
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            dataType: 'JSON',
            success: function (data) {
              if (data.status == "400") {
                swal({
                  title: "Cek!",
                  text: "Cek Kondisi Form!",
                  icon: "warning",
                });
              } else {
                $("#form_add_pegawai")[0].reset();
                $('#modal_add_pegawai').modal('hide');
                $('#datatable_data_pegawai').DataTable().ajax.reload(null, false).draw();
                swal({
                  title: "Berhasil!",
                  text: "Kamu telah berhasil menambahkan data!",
                  icon: "success",
                });
              }
            },
          });
        });
      }
    })
    $("#datatable_data_pegawai").on('click', '#delete_pegawai', function (e) {
      swal({
          title: "Apakah yakin? ",
          text: "Data telah terhapus, kamu tidak akan bisa untuk mengembalikan data yang telah di hapus!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            const id_pegawai = $(this).data('id_pegawai');

            $.ajax({
              url: url + 'backend/pegawai/delete_pegawai.php',
              type: 'POST',
              data: {
                id_pegawai: id_pegawai
              },
              dataType: 'JSON',

              success: function (data) {
                if (data.status == "400") {
                  swal(data.response, {
                    icon: "error",
                  });
                } else {
                  swal(data.response, {
                    icon: "success",
                  });
                  $('#datatable_data_pegawai').DataTable().ajax.reload(null, false).draw();
                }
              },

            })
          } else {
            swal("Data Tidak Jadi di Hapus");
          }
        });
    });
    $("#datatable_data_pegawai").on('click', '#edit_pegawai', function (e) {
      $('#modal_edit_pegawai').modal('show');
      const id_pegawai = $(this).data('id_pegawai');
      $.ajax({
        url: url + 'backend/pegawai/edit_pegawai.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_pegawai: id_pegawai
        },
        success: function (data) {
          $('#id_edit').val(data.id)
          $('#nik_edit').val(data.nik)
          $('#nama_edit').val(data.nama)
          $('#alamat_edit').val(data.alamat)
          $('#nip_edit').val(data.nip)
          $('#jabatan_edit').val(data.jabatan);
        }
      })
    })
    $("#datatable_data_pegawai").on('click', '#detail_pegawai', function (e) {
      $('#modal_detail_pegawai').modal('show');
      const id_pegawai = $(this).data('id_pegawai');
      $.ajax({
        url: url + 'backend/pegawai/edit_pegawai.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_pegawai: id_pegawai
        },
        success: function (data) {
          $('#id_detail').val(data.id)
          $('#nik_detail').val(data.nik)
          $('#nama_detail').val(data.nama)
          $('#alamat_detail').val(data.alamat)
          $('#nip_detail').val(data.nip)
          $('#jabatan_detail').val(data.jabatan);
        }
      })
    })
    
    $("#form_edit_surat_usaha").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/sUsaha/update_usaha.php',
        data: new FormData(this),
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "200") {
            $("#form_edit_surat_usaha")[0].reset();
            $('#modal_edit_pegawai').modal('hide');
            $('#datatable_surat_usaha').DataTable().ajax.reload(null, false).draw();
            $('#formEditSuratUsaha').hide('slow');
            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil mengupdate data!",
              icon: "success",
            });
          } else {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          }
        },
      });
    });
    $("#form_edit_pegawai").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/pegawai/update_pegawai.php',
        data: new FormData(this),
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "200") {
            $("#form_edit_pegawai")[0].reset();
            $('#modal_edit_pegawai').modal('hide');
            $('#datatable_data_pegawai').DataTable().ajax.reload(null, false).draw();
            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil mengupdate data!",
              icon: "success",
            });
          } else {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          }
        },
      });
    });
    $("#buatSurat").click(function () {
      $("#formSuratKelahiran").slideToggle("slow");
    });
    $("#buatMutasi").click(function () {
      $("#formMutasi").slideToggle("slow");
    });
    $("#buatSurat").click(function () {
      $("#formSuratMeninggal").slideToggle("slow");
    });
    $("#buatSurat").click(function () {
      $("#formSuratCerai").slideToggle("slow");
    });
    $("#buatSurat").click(function () {
      $("#formSuratUsaha").slideToggle("slow");
    });
    $("#buatSurat").click(function () {
      $("#formSuratBelumNikah").slideToggle("slow");
    });
    $('#nik_ayah').change(function () {
      let nik_ayah = $('#nik_ayah').val();
      $.ajax({
        url: url + 'backend/penduduk/getTablePendudukAyah.php',
        dataType: 'json',
        data: {
          nik_ayah: nik_ayah
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('#toggleAyah').hide('slow');
            $('#nama_ayah').html('');
            $('#jk_ayah').html('');
            $('#alamat_ayah').html('');
          } else {
            $('#toggleAyah').show('slow');
            $('#nama_ayah').html(data.nama);
            $('#jk_ayah').html(data.jk);
            $('#alamat_ayah').html(data.alamat);
          }
        }
      });
    });
    $('#nik_ibu').change(function () {
      let nik_ibu = $('#nik_ibu').val();
      $.ajax({
        url: url + 'backend/penduduk/getTablePendudukIbu.php',
        dataType: 'json',
        data: {
          nik_ibu: nik_ibu
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('#toggleIbu').hide('slow');
            $('#nama_ibu').html('');
            $('#jk_ibu').html('');
            $('#alamat_ibu').html('');
          } else {
            $('#toggleIbu').show('slow');
            $('#nama_ibu').html(data.nama);
            $('#jk_ibu').html(data.jk);
            $('#alamat_ibu').html(data.alamat);
          }
        }
      });
    });
    $('#nik_pemohon').change(function () {
      let nik_pemohon = $('#nik_pemohon').val();
      $.ajax({
        url: url + 'backend/sCerai/getTablePendudukNikPemohon.php',
        dataType: 'json',
        data: {
          nik_pemohon: nik_pemohon
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('#togglePemohon').hide('slow');
            $('#nama_pemohon').html('');
            $('#jk_pemohon').html('');
            $('#alamat_pemohon').html('');
          } else {
            $('#togglePemohon').show('slow');
            $('#nama_pemohon').html(data.nama);
            $('#jk_pemohon').html(data.jk);
            $('#alamat_pemohon').html(data.alamat);
          }
        }
      });
    });
    $('#nik_pasangan').change(function () {
      let nik_pasangan = $('#nik_pasangan').val();
      $.ajax({
        url: url + 'backend/sCerai/getTablePendudukNikPasangan.php',
        dataType: 'json',
        data: {
          nik_pasangan: nik_pasangan
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('#togglePasangan').hide('slow');
            $('#nama_pasangan').html('');
            $('#jk_pasangan').html('');
            $('#alamat_pasangan').html('');
          } else {
            $('#togglePasangan').show('slow');
            $('#nama_pasangan').html(data.nama);
            $('#jk_pasangan').html(data.jk);
            $('#alamat_pasangan').html(data.alamat);
          }
        }
      });
    });
    $("#form_add_surat_kelahiran").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/sKelahiran/tambah_surat_kelahiran.php',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "400") {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          } else {
            $("#formSuratKelahiran").hide("slow");
            $('#toggleAyah').hide('slow');
            $('#nama_ayah').html('');
            $('#jk_ayah').html('');
            $('#alamat_ayah').html('');
            $('#toggleIbu').hide('slow');
            $('#nama_ibu').html('');
            $('#jk_ibu').html('');
            $('#alamat_ibu').html('');
            $("#form_add_surat_kelahiran")[0].reset();
            $('#jk').selectpicker('val', '');
            $('#agama').selectpicker('val', '');

            $('#datatable_surat_kelahiran').DataTable().ajax.reload(null, false).draw();

            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil menambahkan data!",
              icon: "success",
            });
          }
        },
      });
    });
    $("#form_add_mutasi").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/mutasi/tambah_mutasi.php',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "400") {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          } else {
            $("#formMutasi").hide("slow");
            $('#toggleDetailMutasi').hide('slow');
            $('#detail_nama').html('');
            $('#detail_jk').html('');
            $('#detail_tempat').html('');
            $("#form_add_mutasi")[0].reset();
            $('#jenis_mutasi').selectpicker('val', '');
            $('#kategori_kepindahan').selectpicker('val', '');
            $('#datatable_mutasi').DataTable().ajax.reload(null, false).draw();

            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil menambahkan data!",
              icon: "success",
            });
          }
        },
      });
    });
    $("#form_add_surat_cerai").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/sCerai/tambah_surat_cerai.php',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "400") {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          } else {
            $("#formSuratCerai").hide("slow");

            $('#no_surat').html('');
            $('#niK_pemohon').html('');
            $('#niK_pasangan').html('');
            $('#tgl_cerai').html('');

            $("#form_add_surat_cerai")[0].reset();

            $('#datatable_surat_cerai').DataTable().ajax.reload(null, false).draw();

            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil menambahkan data!",
              icon: "success",
            });
          }
        },
      });
    });

    $("#datatable_surat_kelahiran").on('click', '#delete_skelahiran', function (e) {
      swal({
          title: "Apakah yakin? ",
          text: "Data telah terhapus, kamu tidak akan bisa untuk mengembalikan data yang telah di hapus!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            const id_surat = $(this).data('id_surat');

            $.ajax({
              url: url + 'backend/sKelahiran/delete_skelahiran.php',
              type: 'POST',
              data: {
                id_surat: id_surat
              },
              dataType: 'JSON',

              success: function (data) {
                if (data.status == "400") {
                  swal(data.response, {
                    icon: "error",
                  });
                } else {
                  swal(data.response, {
                    icon: "success",
                  });
                  $('#datatable_surat_kelahiran').DataTable().ajax.reload(null, false).draw();
                }
              },

            })
          } else {
            swal("Data Tidak Jadi di Hapus");
          }
        });
    })
    $("#datatable_surat_kelahiran").on('click', '#edit_skelahiran', function (e) {
      $("#formEditSuratKelahiran").slideToggle("slow");
      const id_surat = $(this).data('id_surat');
      $.ajax({
        url: url + 'backend/sKelahiran/edit_skelahiran.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_surat: id_surat
        },
        success: function (data) {          
          $('#id_edit').val(data.id)
          $('#no_surat_edit').val(data.no_surat)
          $('#nama_anak_edit').val(data.nama_anak)
          $('#jk_edit').selectpicker('val', data.jk);
          $('#tmp_lahir_edit').val(data.tmp_lahir)
          $('#tgl_lahir_edit').val(data.tgl_lahir)
          $('#agama_edit').selectpicker('val', data.agama);
          $('#alamat_edit').val(data.alamat)
          $('#anakke_edit').val(data.anakke)
          $('#nik_ayah_edit').val(data.nik_ayah)
          $('#nik_ibu_edit').val(data.nik_ibu)
        }
      })
    })
    $("#form_edit_surat_kelahiran").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/skelahiran/update_skelahiran.php',
        data: new FormData(this),
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "200") {
            $('#jk_edit').selectpicker('val', '');
            $('#agama_edit').selectpicker('val', '');
            $("#form_edit_surat_kelahiran")[0].reset();
            $("#formEditSuratKelahiran").hide("slow");
            $('#datatable_surat_kelahiran').DataTable().ajax.reload(null, false).draw();
            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil mengupdate data!",
              icon: "success",
            });
          } else {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          }
        },
      });
    });
    //----------------------------------------------------------------------------------\\    
    $('#cari_nik').change(function () {
      let param = $('#cari_nik').val();
      $.ajax({
        url: url + 'backend/mutasi/getTablePenduduk.php',
        dataType: 'json',
        data: {
          cari_nik: param
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('#toggleDetailMutasi').hide('slow');
            $('#detail_nama').html('');
            $('#detail_jk').html('');
            $('#detail_tempat').html('');
          } else {
            $('#toggleDetailMutasi').show('slow');
            $('#detail_nama').html(data.nama);
            $('#detail_jk').html(data.jk);
            $('#detail_tempat').html(data.alamat);
            $('#alamat_asal').html(data.alamat);
          }
        }
      });
    });
    $('.cari_nik_sketusaha').change(function () {
      let cari_nik = $('.cari_nik_sketusaha').val();      
      $.ajax({
        url: url + 'backend/mutasi/getTablePenduduk.php',
        dataType: 'json',
        data: {
          cari_nik: cari_nik
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('.togglePemohon').hide('slow');
            $('.detail_nama').html('');
            $('.detail_jk').html('');
            $('.detail_tempat').html('');
          } else {
            $('.togglePemohon').show('slow');
            $('.detail_nama').html(data.nama);
            $('.detail_jk').html(data.jk);
            $('.detail_tempat').html(data.alamat);
          }
        }
      });
    });
    $('.cari_nik_belum_nikah').change(function () {
      let cari_nik = $('.cari_nik_belum_nikah').val();      
      $.ajax({
        url: url + 'backend/mutasi/getTablePenduduk.php',
        dataType: 'json',
        data: {
          cari_nik: cari_nik
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('.togglePemohon').hide('slow');
            $('.detail_nama').html('');
            $('.detail_jk').html('');
            $('.detail_tempat').html('');
            $('.detail_sPerkawinan').html('');
          } else {
            $('.togglePemohon').show('slow');
            $('.detail_nama').html(data.nama);
            $('.detail_jk').html(data.jk);
            $('.detail_tempat').html(data.alamat);
            $('.detail_sPerkawinan').html(data.sperkawinan);
          }
        }
      });
    });
    $('.cari_nik_sketusaha_edit').change(function () {
      let cari_nik = $('.cari_nik_sketusaha_edit').val();      
      $.ajax({
        url: url + 'backend/mutasi/getTablePenduduk.php',
        dataType: 'json',
        data: {
          cari_nik: cari_nik
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {            
            $('.detail_nama_edit').html('');
            $('.detail_jk_edit').html('');
            $('.detail_tempat_edit').html('');
          } else {
            $('.togglePemohon').show('slow');
            $('.detail_nama_edit').html(data.nama);
            $('.detail_jk_edit').html(data.jk);
            $('.detail_tempat_edit').html(data.alamat);
          }
        }
      });
    });

    //----------------------------------------------------------------------------------\\
    $('#nik_meninggal').change(function () {
      let param = $('#nik_meninggal').val();
      $.ajax({
        url: url + 'backend/sMeninggal/getTablePendudukParam.php',
        dataType: 'json',
        data: {
          nik_meninggal: param
        },
        type: "post",
        success: function (data) {
          if (data.response == 'Data Kosong') {
            $('#toggleMeninggal').hide('slow');
            $('#id_meninggal').html('');
            $('#jk_meninggal').html('');
            $('#alamat_meninggal').html('');
          } else {
            $('#toggleMeninggal').show('slow');
            $('#id_meninggal').html(data.id);
            $('#nama_meninggal').html(data.nama);
            $('#jk_meninggal').html(data.jk);
            $('#alamat_meninggal').html(data.alamat);
          }
        }
      });
    });
    $("#form_add_surat_meninggal").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/sMeninggal/tambah_surat_meninggal.php',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "400") {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          } else {
            $("#formSuratMeninggal").hide("slow");
            $('#toggleMeninggal').hide('slow');
            $("#form_add_surat_meninggal")[0].reset();

            $('#datatable_surat_meninggal').DataTable().ajax.reload(null, false).draw();

            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil menambahkan data!",
              icon: "success",
            });
          }
        },
      });
    });
    $("#form_add_surat_usaha").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/sUsaha/tambah_surat_usaha.php',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "400") {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          } else {
            $("#formSuratUsaha").hide("slow");
            $('#togglePemohon').hide('slow');
            $("#form_add_surat_usaha")[0].reset();

            $('#datatable_surat_usaha').DataTable().ajax.reload(null, false).draw();

            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil menambahkan data!",
              icon: "success",
            });
          }
        },
      });
    });

    $("#form_add_surat_belum_nikah").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/sBelumNikah/tambah_surat_belum_nikah.php',
        data: new FormData(this),
        contentType: false,
        cache: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "400") {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          } else {
            $("#formSuratBelumNikah").hide("slow");
            $('#togglePemohon').hide('slow');
            $("#form_add_surat_belum_nikah")[0].reset();

            $('#datatable_surat_belum_nikah').DataTable().ajax.reload(null, false).draw();

            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil menambahkan data!",
              icon: "success",
            });
          }
        },
      });
    });

    $("#datatable_surat_kelahiran").on('click', '#delete_skelahiran', function (e) {
      swal({
          title: "Apakah yakin? ",
          text: "Data telah terhapus, kamu tidak akan bisa untuk mengembalikan data yang telah di hapus!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
        .then((willDelete) => {
          if (willDelete) {
            const id_surat = $(this).data('id_surat');

            $.ajax({
              url: url + 'backend/sKelahiran/delete_skelahiran.php',
              type: 'POST',
              data: {
                id_surat: id_surat
              },
              dataType: 'JSON',

              success: function (data) {
                if (data.status == "400") {
                  swal(data.response, {
                    icon: "error",
                  });
                } else {
                  swal(data.response, {
                    icon: "success",
                  });
                  $('#datatable_surat_kelahiran').DataTable().ajax.reload(null, false).draw();
                }
              },

            })
          } else {
            swal("Data Tidak Jadi di Hapus");
          }
        });
    })
    $("#datatable_surat_kelahiran").on('click', '#edit_skelahiran', function (e) {
      const id_surat = $(this).data('id_surat');
      $.ajax({
        url: url + 'backend/sKelahiran/edit_skelahiran.php',
        type: 'POST',
        dataType: 'JSON',
        data: {
          id_surat: id_surat
        },
        success: function (data) {
          $('#id_edit').val(data.id)
          $('#no_surat_edit').val(data.no_surat)
          $('#nama_anak_edit').val(data.nama_anak)
          $('#jk_edit').selectpicker('val', data.jk);
          $('#tmp_lahir_edit').val(data.tmp_lahir)
          $('#tgl_lahir_edit').val(data.tgl_lahir)
          $('#agama_edit').selectpicker('val', data.agama);
          $('#alamat_edit').val(data.alamat)
          $('#anakke_edit').val(data.anakke)
          $('#nik_ayah').val(data.nik_ayah)
          $('#nik_ibu').val(data.nik_ibu)
        }
      })
    })
    $("#form_edit_surat_kelahiran").submit(function (e) {
      e.preventDefault();
      $.ajax({
        type: 'POST',
        url: url + 'backend/skelahiran/update_skelahiran.php',
        data: new FormData(this),
        contentType: false,
        processData: false,
        dataType: 'JSON',
        success: function (data) {
          if (data.status == "200") {
            $('#jk_edit').selectpicker('val', '');
            $('#agama_edit').selectpicker('val', '');
            $("#form_edit_surat_kelahiran")[0].reset();
            $("#formEditSuratKelahiran").hide("slow");
            $('#datatable_surat_kelahiran').DataTable().ajax.reload(null, false).draw();
            swal({
              title: "Berhasil!",
              text: "Kamu telah berhasil mengupdate data!",
              icon: "success",
            });
          } else {
            swal({
              title: "Cek!",
              text: "Cek Kondisi Form!",
              icon: "warning",
            });
          }
        },
      });
    });
  })