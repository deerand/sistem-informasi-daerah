$(document).ready(function () {
  var url = window.location.href.split('/')[0] + '/';
  var skawinGraph = document.getElementById("sKawinGraph");

  $.ajax({
    url: url + 'backend/graph/_sKawinGraph.php',
    type: 'GET',
    dataType: 'JSON',
    success: function (data) {
      let getKey = Object.keys(data);
      let getInt = Object.keys(data).length;

      var mapped = Object.keys(data).map(function (uid) {
        return parseInt((data[uid].uid = uid) && data[uid]);
      });
      sKawinChart.data.datasets.map(function (o) {
        o.data = mapped;
        return o;
      })
      sKawinChart.data.labels = ["Kawin","Belum Kawin","Cerai"];
      sKawinChart.update();
      var total = 0;
      for (var i = 0; i < mapped.length; i++) {
        total += mapped[i] << 0;
      }
      var y = 0;      
      for(i= 0;i < getInt; i++)
      {
        y++;
        $('#persentaseSKawin').append("<tr><td>"+y+"</td><td>"+getKey[i]+"</td><td>"+mapped[i]+"</td><td>"+mapped[i]/total*100+"%</td></tr>")
      }

    }
  });

  var sKawinChart = new Chart(skawinGraph, {
    type: 'bar',
    beginAtZero: true,
    data: {
      datasets: [{
        label: "Grafik Penduduk Berdasarkan Status Perkawinan",
        data: [0],
        backgroundColor: [
          'rgba(255, 0, 0, 0.5)',
          'rgba(0, 255, 0, 0.5)',
          'rgba(0, 0, 255, 0.5)',
          'rgba(255, 255, 0, 0.5)',
        ],
        borderColor: [
          'rgba(255, 0, 0, 1)',
          'rgba(0, 255, 0, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(255, 255, 0, 1)',
        ],
        hoverBackgroundColor: [
          'rgba(255, 0, 0, 1)',
          'rgba(0, 255, 0, 1)',
          'rgba(0, 0, 255, 1)',
          'rgba(255, 255, 0, 1)',
        ],
        borderWidth: 1,
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true,
            callback: function (value) {
              if (Number.isInteger(value)) {
                return value;
              }
            },
            stepSize: 100
          }
        }]
      }

    }
  });
});