<?php

include "../../backend/config/koneksi.php";
session_start();

$valid_extensions = array('jpeg', 'jpg', 'png', 'gif', 'bmp' , 'pdf' , 'doc' , 'ppt'); // valid extensions
$path = '../../images/penduduk/'; // upload directory

$img = $_FILES['image']['name'];
$tmp = $_FILES['image']['tmp_name'];

$ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
$final_image = time()."_".$img;

$path = $path.strtolower($final_image); 

$nip = $_SESSION['nip'];
$nik = $_POST['nik'];
$nama = $_POST['nama'];
$alamat = $_POST['alamat'];
$jk = $_POST['jk'];
$tmp_lahir = $_POST['tmp_lahir'];
$tgl_lahir = $_POST['tgl_lahir'];
$aktelahir = $_POST['aktelahir'];
$no_aktelahir = $_POST['no_aktelahir'];
$agama = $_POST['agama'];
$pendidikan = $_POST['pendidikan'];
$pekerjaan = $_POST['pekerjaan'];
$penghasilan = $_POST['penghasilan'];
$sDalamKeluarga = $_POST['sDalamKeluarga'];
$sPerkawinan = $_POST['sPerkawinan'];
$aktekawin = $_POST['aktekawin'];
$no_aktekawin = $_POST['no_aktekawin'];
$tgl_aktekawin = $_POST['tgl_aktekawin'];
$aktecerai = $_POST['aktecerai'];
$no_aktecerai = $_POST['no_aktecerai'];
$tgl_aktecerai = $_POST['tgl_aktecerai'];
$goldar = $_POST['goldar'];
$kelainanFM = $_POST['kelainanFM'];
$status_hidup = $_POST['status_hidup'];
$datenow = date('Y-m-d');

$query = mysqli_query($con,"INSERT INTO penduduk (
    nip_pegawai,
    nik,
    nama,
    alamat,
    jk,
    tmp_lahir,
    tgl_lahir,
    aktelahir,
    no_aktelahir,
    agama,pendidikan,
    pekerjaan,
    penghasilan,
    sDalamKeluarga,
    sPerkawinan,
    aktekawin,
    no_aktekawin,
    tgl_aktekawin,
    aktecerai,
    no_aktecerai,
    tgl_aktecerai,
    goldar,
    kelainanFM,
    status_hidup,
    foto,
    created_at)
VALUES(
    '$nip',
    '$nik',
    '$nama',
    '$alamat',
    '$jk',
    '$tmp_lahir',
    '$tgl_lahir',
    '$aktelahir',
    '$no_aktelahir',
    '$agama',
    '$pendidikan',
    '$pekerjaan',
    $penghasilan,
    '$sDalamKeluarga',
    '$sPerkawinan',
    '$aktekawin',
    '$no_aktekawin',
    '$tgl_aktekawin',
    '$aktecerai',
    '$no_aktecerai',
    '$tgl_aktecerai',
    '$goldar',
    '$kelainanFM',
    '$status_hidup',
    '$final_image',
    '$datenow')") or die (mysqli_error($con));
    
    if($con->errno)
    {
        $response = array (
            'response' => 'Test',
            'status' => 400
        );
    } else 
    {
        move_uploaded_file($tmp,$path);
        $response = array (
            'response' => 'Test',
            'status' => 200
        );
    }
    echo json_encode($response);
?>