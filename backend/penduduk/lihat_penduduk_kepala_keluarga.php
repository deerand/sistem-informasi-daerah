<?php
include "../../backend/config/koneksi.php";
$requestData= $_REQUEST;
$columns = array( 
    // datatable column index  => database column name
    0 => 'no',    
    1 => 'nik',
    2 => 'nama',
    3 => 'jk',
    4 => 'tmp_lahir',
	5 => 'tgl_lahir',
	6 => 'id'
);
// getting total number records without any search
$sql = "SELECT id,nik,nama,jk,tmp_lahir,tgl_lahir FROM penduduk WHERE sDalamKeluarga='Kepala Keluarga' ORDER BY id";
$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT id,nik,nama,jk,tmp_lahir,tgl_lahir FROM penduduk WHERE";
    $sql.=" (sDalamKeluarga='Kepala Keluarga' AND nik LIKE '".$requestData['search']['value']."%')";
	$sql.=" OR (sDalamKeluarga='Kepala Keluarga' AND nama LIKE '".$requestData['search']['value']."%')";
	$sql.=" OR (sDalamKeluarga='Kepala Keluarga' AND jk LIKE '".$requestData['search']['value']."%')";
    $sql.=" OR (sDalamKeluarga='Kepala Keluarga' AND tmp_lahir LIKE '".$requestData['search']['value']."%')";
    $sql.=" OR (sDalamKeluarga='Kepala Keluarga' AND tgl_lahir LIKE '".$requestData['search']['value']."%')";
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees"); // again run query with limit
	
	} else {	
    $sql = "SELECT id,nik,nama,jk,tmp_lahir,tgl_lahir FROM penduduk WHERE sDalamKeluarga='Kepala Keluarga'";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($con, $sql) or die("employee-grid-data.php: get employees");
	
}
$data = array();
$no =1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
  $nestedData=array();
  $nestedData['no'] = $no++;
  $nestedData['id'] = $row["id"];
  $nestedData['nik'] = $row["nik"];
	$nestedData['nama'] = $row["nama"];
  $nestedData['jk'] = $row["jk"];	
  $nestedData['tmp_lahir'] = $row["tmp_lahir"];	
	$nestedData['tgl_lahir'] = $row["tgl_lahir"];
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>