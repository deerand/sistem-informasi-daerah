<?php

include "../../backend/config/koneksi.php";
$query = "SELECT 
            COUNT(IF(agama='Islam',1, NULL)) 'Islam',
            COUNT(IF(agama='Kristen',1, NULL)) 'Kristen',
            COUNT(IF(agama='Katolik',1, NULL)) 'Katolik',
            COUNT(IF(agama='Hindu',1, NULL)) 'Hindu',
            COUNT(IF(agama='Budha',1, NULL)) 'Budha',
            COUNT(IF(agama='Khonghucu',1, NULL)) 'Khonghucu',
            COUNT(IF(agama='Kepercayaan',1, NULL)) 'Kepercayaan'
          FROM penduduk";
$sql = mysqli_query($con,$query);
while($data = mysqli_fetch_array($sql))
{    
    $json_data['Islam'] = $data['Islam'];
    $json_data['Kristen'] = $data['Kristen'];
    $json_data['Katolik'] = $data['Katolik'];
    $json_data['Hindu'] = $data['Hindu'];
    $json_data['Budha'] = $data['Budha'];
    $json_data['Khonghucu'] = $data['Khonghucu'];
    $json_data['Kepercayaan'] = $data['Kepercayaan'];
}
$json_array = $json_data;
echo json_encode($json_array);