<?php

include "../../backend/config/koneksi.php";

$requestData= $_REQUEST;
$columns = array( 
  // datatable column index  => database column name
	0 => 'no',
  1 => 'no_surat',
  2 => 'nama',
  3 => 'tgl_meninggal',
  4 => 'jk',
  5 => 'nama_pegawai',
  6 => 'id',
);
// getting total number records without any search
$sql = "SELECT smeninggal.id,smeninggal.no_surat,penduduk.nama,smeninggal.tgl_meninggal,penduduk.jk,pegawai.nama AS nama_pegawai FROM smeninggal INNER JOIN pegawai ON smeninggal.nip_pegawai=pegawai.nip INNER JOIN penduduk ON smeninggal.nik_meninggal=penduduk.nik ORDER BY smeninggal.id";
$query=mysqli_query($con, $sql) or die(mysqli_error($con));
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  // when there is no search parameter then total number rows = total number filtered rows.
if( !empty($requestData['search']['value']) ) {
	// if there is a search parameter
	$sql = "SELECT smeninggal.id,smeninggal.no_surat,penduduk.nama,smeninggal.tgl_meninggal,penduduk.jk,pegawai.nama AS nama_pegawai FROM smeninggal INNER JOIN pegawai ON smeninggal.nip_pegawai=pegawai.nip INNER JOIN penduduk ON smeninggal.nik_meninggal=penduduk.nik";
	$sql.=" WHERE no_surat LIKE '".$requestData['search']['value']."%' ";    // $requestData['search']['value'] contains search parameter
	$sql.=" OR nama LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR tgl_meninggal LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR jk LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR pegawai.nama LIKE '".$requestData['search']['value']."%' ";
	$sql.=" ORDER BY smeninggal.id ";
	
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result without limit in the query 
	$sql.="".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	
	} else {	
    $sql = "SELECT smeninggal.id,smeninggal.no_surat,penduduk.nama,smeninggal.tgl_meninggal,penduduk.jk,pegawai.nama AS nama_pegawai FROM smeninggal INNER JOIN pegawai ON smeninggal.nip_pegawai=pegawai.nip INNER JOIN penduduk ON smeninggal.nik_meninggal=penduduk.nik";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($con, $sql) or die(mysqli_error($con));
	
}
$data = array();
$no =1;
while( $row=mysqli_fetch_array($query) ) {  // preparing an array
    $nestedData=array();
    
	$nestedData['no'] = $no++;
	$nestedData['no_surat'] = $row["no_surat"];
	$nestedData['nama'] = $row["nama"];
	$nestedData['tgl_meninggal'] = $row["tgl_meninggal"];
	$nestedData['jk'] = $row["jk"];
	$nestedData['nama_pegawai'] = $row["nama_pegawai"];
	$nestedData['id'] = $row["id"];
	
	$data[] = $nestedData;
}
$json_data = array(
			"draw"            => intval( $requestData['draw'] ),   // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw. 
			"recordsTotal"    => intval( $totalData ),  // total number of records
			"recordsFiltered" => intval( $totalFiltered ), // total number of records after searching, if there is no searching then totalFiltered = totalData
			"data"            => $data   // total data array
			);
echo json_encode($json_data);  // send data as json format
?>