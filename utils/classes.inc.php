<?php

class Auth
{
    static public function isLoggedIn()
    {
        if (isset($_SESSION['nip'])) {
            return true;
        }

        return false;
    }
}

class Response
{
    static public function json(Array $res = [])
    {
        header('Content-Type: application/json');
        echo json_encode($res);
        return;
    }

    static public function redirect($url, $statusCode = 303)
    {
        header('Location: ' . $url, true, $statusCode);
        die();
    }
}

/*  Example Case

    Request::get("handler_name", function() {
        echo "It's work!";
    });
    Request::post("handler_name", function() {
        echo "It's work!";
    });
    Request::update("handler_name", function() {
        echo "It's work!";
    });
    Request::delete("handler_name", function() {
        echo "It's work!";
    });
*/
?>