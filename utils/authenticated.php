<?php
session_start();

include_once "classes.inc.php";

Auth::isLoggedIn() ? false : Response::redirect('/login.php');